<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salaryModel extends Model
{
    protected $table='salary';
    protected $fillable=['staff_id','staff_name','s_type','m_status','designation','salary','performance_allowance','expensive_allowance','medical_allowance','communication_allowance','waste_mgmt','advance','pro_fund','less_pf','tax','paid_up_salary','date','bonus_amount','comission_amount','leave_amount','food_amount','overtime_amount','additional_deduct','grand_total','remarks'];
    
    public function staffs(){
    	return $this->belongsTo(staffModel::class,'staff_id');
    }
}
