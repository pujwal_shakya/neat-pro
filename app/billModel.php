<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class billModel extends Model
{
    protected $table='tbl_bill';
    protected $fillable=['customer_id','customer_code','customer_name','marga_id','staff_id','bill_no','date','paid_from','paid_upto','monthly_rate','paid_amount','member_fee','card_fee','drain_clean_fee','late_fee','other_fee','service_charge','less_service_charge','less_discount','total','remarks'];

    public function customers(){
    	return $this->belongsTo(customerModel::class,'customer_id');
    }
    public function margas(){
    	return $this->belongsTo(margaModel::class,'marga_id');
    }
    public function staffs(){
    	return $this->belongsTo(staffModel::class,'staff_id');
    }
}
