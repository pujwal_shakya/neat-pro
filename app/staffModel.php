<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class staffModel extends Model
{
    Protected $table='staff';
    Protected $fillable=['name','designation_id','gender','m_status','s_type','phone','address','designation','salary','performance_allowance','expensive_allowance','medical_allowance','communication_allowance','waste_mgmt','image'];

    public function margas(){
    	return $this->hasMany('App\margaModel','staff_id');
    }
    public function customers(){
    	return $this->hasMany('App\customerModel','staff_id');
    }
    public function salaries(){
    	return $this->hasMany('App\salaryModel','staff_id');
    }
    public function designations(){
        return $this->belongsTo('App\designationModel','designation_id');
    }
    public function advSalaries(){
        return $this->hasMany(advSalaryModel::class,'staff_id');
    }
    public function banks(){
        return $this->hasMany(bankModel::class,'staff_id');
    }
    public function collections(){
        return $this->hasMany(collectionModel::class,'staff_id');
    }
    public function bills(){
        return $this->hasMany(billModel::class,'staff_id');
    }
    public function festivals(){
        return $this->hasMany(festivalModel::class,'staff_id');
    }
}
