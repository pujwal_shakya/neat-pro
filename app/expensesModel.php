<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expensesModel extends Model
{
    protected $table='tbl_expenses';
    protected $fillable=['date','v_no','title','Amount','particular'];
}
