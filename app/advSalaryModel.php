<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class advSalaryModel extends Model
{
    protected $table='tbl_advance';
    protected $fillable= ['staff_id','basic_salary','date','amount','status','remarks'];

    public function staffs(){
    	return $this->belongsTo(staffModel::class,'staff_id');
    }
}
