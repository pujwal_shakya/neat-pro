<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reportModel extends Model
{
    protected $table='tbl_annual_report';
    protected $fillable=['customer_code','customer_id','customer_name','marga_id','staff_id','bill_1','bill_2','bill_3','bill_4','bill_5','bill_6','bill_7','bill_8','bill_9','bill_10','bill_11','bill_12','date_1','date_2','date_3','date_4','date_5','date_6','date_7','date_8','date_9','date_10','date_11','date_12','amount_1','amount_2','amount_3','amount_4','amount_5','amount_6','amount_7','amount_8','amount_9','amount_10','amount_11','amount_12','festival_bill','festival_date','festival_amount'];

    public function customers(){
    	return $this->belongsTo(customerModel::class,'customer_id');
    }
    public function margas(){
    	return $this->belongsTo(margaModel::class,'marga_id');
    }
}
