<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bankModel extends Model
{
    protected $table='tbl_bank';
    protected $fillable=['bank_name','branch','acc_no','acc_name','acc_type','deposit_type','transaction','staff_id','amount','remarks','date'];

    public function staffs(){
    	return $this->belongsTo(staffModel::class,'staff_id');
    }
}
