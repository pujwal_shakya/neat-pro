<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerModel extends Model
{
    Protected $table='customer';
    Protected $fillable=['code_no','name','phone','mobile','house_no','ward_no','marga_id','staff_id','kitchen_no','shutter_no','floor_no','monthly_charge','status','date'];

    public function margas(){
    	return $this->belongsTO(margaModel::class,'marga_id');
    }
    public function staffs(){
    	return $this->belongsTO(staffModel::class,'staff_id');
    }
    public function bills(){
    	return $this->hasMany(billModel::class,'customer_id');
    }
    public function festivals(){
        return $this->hasMany(festivalModel::class,'customer_id');
    }
    public function reports(){
        return $this->hasMany(reportModel::class,'customer_id');
    }
}
