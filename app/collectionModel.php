<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class collectionModel extends Model
{
    Protected $table='tbl_collection';
    Protected $fillable=['date','bill_from','bill_to','dr_amt','cr_amt','balance','previous_collection','no_of_bill','staff_id','status'];

    public function staffs(){
    	return $this->belongsTo(staffModel::class,'staff_id');
    }
}
