<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class festivalModel extends Model
{
    protected $table='tbl_festival';
    protected $fillable=['customer_id','customer_code','customer_name','marga_id','staff_id','bill_no','date','paid_amount','remarks'];

    public function customers(){
    	return $this->belongsTo(customerModel::class,'customer_id');
    }
    public function margas(){
    	return $this->belongsTo(margaModel::class,'marga_id');
    }
    public function staffs(){
    	return $this->belongsTo(staffModel::class,'staff_id');
    }
}
