<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class margaModel extends Model
{
    protected $table='marga';
    protected $fillable=['name','staff_id'];

    public function staffs(){
    	return $this->belongsTO(staffModel::class,'staff_id');
    }
    public function customers(){
    	return $this->hasMany(customerModel::class,'marga_id');
    }
    public function bills(){
    	return $this->hasMany(billModel::class,'marga_id');
    }
    public function festivals(){
        return $this->hasMany(festivalModel::class,'marga_id');
    }
    public function reports(){
        return $this->hasMany(reportModel::class,'marga_id');
    }
}
