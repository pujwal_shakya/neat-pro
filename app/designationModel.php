<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class designationModel extends Model
{
    protected $table='tbl_designation';
    protected $fillable=['designation'];

    public function staffs(){
    	return $this->hasMany(staffModel::class,'designation_id');
    }
}
