<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\expensesModel;

class expensesController extends Controller
{
    public function index(){
    	$expenses=expensesModel::all();
    	return view('expenses.expenses',compact('expenses'));
    }
    public function addExpenses(){
    	return view('expenses.addExpenses');
    }
    public function saveExpenses(Request $request){
    	$this->validate($request,array(
    		'date'=>'required',
    		'v_no'=>'required|unique:tbl_expenses,v_no',
    		'title'=>'required'
    	));
    	$result=expensesModel::create([
    		'date'=>$request->date,
    		'v_no'=>$request->v_no,
    		'title'=>$request->title,
    		'Amount'=>$request->amount,
    		'particular'=>$request->particulars
    	]);
    	if($result){
    		Session::flash('success','Expenses Stored');
    	}
    	else{
    		Session::flash('error','Expenses Cannot Be Stored');
    	}
    	return redirect()->back();
    }
    public function delete($id){
    	expensesModel::find($id)->delete();
    	return redirect()->back();
    }
    public function editExpenses($id){
    	$expenses=expensesModel::find($id);
    	return view('expenses.editExpenses',compact('expenses'));
    }
    public function updateExpenses(Request $request,$id){
    	$this->validate($request,array(
    		'date'=>'required',
    		'v_no'=>['required',Rule::unique('tbl_expenses','v_no')->ignore($id)],
    		'title'=>'required'
    	));
    	$result=expensesModel::find($id)->update([
    		'date'=>$request->date,
    		'v_no'=>$request->v_no,
    		'title'=>$request->title,
    		'Amount'=>$request->amount,
    		'particular'=>$request->particulars
    	]);
    	if($result){
    		Session::flash('success','Expenses Updated');
    	}
    	else{
    		Session::flash('error','Expenses Cannot Be Updated');
    	}
    	return redirect()->back();
    }
}
