<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customerModel;
use App\staffModel;
use App\margaModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Session;
use Yajra\Datatables\Datatables;


class customerController extends Controller
{
    public function customer(){
        $margas=margaModel::all();
        $staffs=staffModel::all();
        $customers=customerModel::with('margas.staffs')->get();
        return view('customer.customer',compact('customers','staffs','margas'));
    }

    public function addCustomer(){
    	$margas=margaModel::all();
    	$staffs=staffModel::all();
        $customers=customerModel::all();  
        $customer_id=customerModel::orderby('id','desc')->pluck('id')->first(); 
        return view('customer.addCustomer',compact('margas','staffs','customers','customer_id'));
    }
    public function saveCustomer(Request $request){
        $this->validate($request,array(
            'code_no'=>'required|unique:customer,code_no',
            'name'=>'required|max:25',
            'mobile'=>'required|max:10|unique:customer,mobile',
            'house_no'=>'required|unique:customer,house_no',
            'ward_no'=>'required|max:2',
            'monthly_charge'=>'required',
            'date'=>'required|date',
            'marga'=>'required',
        ));
        $result=customerModel::create(['code_no'=>$request->code_no,'name'=>$request->name,'phone'=>$request->phone,'mobile'=>$request->mobile,'house_no'=>$request->house_no,'ward_no'=>$request->ward_no,'marga_id'=>$request->marga,'kitchen_no'=>$request->no_of_kitchen,'shutter_no'=>$request->no_of_shutter,'floor_no'=>$request->no_of_floors,'monthly_charge'=>$request->monthly_charge,'status'=>1,'date'=>$request->date]);
        if($result){
            Session::flash('success','success fully Inserted');

        }else{
         Session::flash('error','cannot  Inserted');
     }

     return redirect()->back();
 }
 public function customerDetail($id){
    $customers=customerModel::find($id);
    $staffs=staffModel::all();
    $margas=margaModel::all();  
    if(!empty($customers)){

        return view('customer.customerDetail',compact('customers','staffs','margas'));

    }
    else{
        return view('errors.500');
    }
}   
public function updateDetail($id, Request $request){
    $this->validate($request,array(
        'code_no'=>['required',Rule::unique('customer','code_no')->ignore($request->id),],
        'name'=>'required|max:25',
        'mobile'=>['required',Rule::unique('customer','mobile')->ignore($request->id),],
        'house_no'=>['required',Rule::unique('customer','house_no')->ignore($request->id),],
        'ward_no'=>'required|max:2',
        'monthly_charge'=>'required',
        'date'=>'required|date',
        'marga'=>'required',
    ));
    $result=customerModel::find($id)->update(['code_no'=>$request->code_no,'name'=>$request->name,'phone'=>$request->phone,'mobile'=>$request->mobile,'house_no'=>$request->house_no,'ward_no'=>$request->ward_no,'marga_id'=>$request->marga,'kitchen_no'=>$request->no_of_kitchen,'shutter_no'=>$request->no_of_shutter,'floor_no'=>$request->no_of_floors,'monthly_charge'=>$request->monthly_charge,'status'=>1,'date'=>$request->date]);
    if($result){
        session::flash('success','Successfully Updated');
    }
    else{
        session::flash('error','Cannot Updated');
    }
    return redirect()->back();
}
public function deleteCustomer($id){
    customerModel::find($id)->delete();
    return redirect()->back();
}
public function margaId($id){
    $margas=margaModel::find($id);
    $staff = $margas->staffs;
    if(empty($staff)){
        return 'no-staff';
    }
    return $staff;
}
public function getData(Datatables $datatables,Request $request){
    $query=customerModel::with('margas.staffs')
    ->when($request->m_name,function($query) use ($request){
        $query->where('marga_id',$request->m_name); 
    })
    ->when($request->s_name,function($query) use ($request){
        $query->whereHas('margas',function($q) use ($request){
            $q->where('staff_id',$request->s_name);
        });
    })
    ->where(function($query)use($request){
        $query->Where('code_no','like','%'.$request->code_no.'%')
        ->Where('name','like','%'.$request->c_name.'%');
    })
    ->get();
    return $datatables->of($query)
    ->addColumn('action', function ($data) {
        return "<a href='".route('customer.detail',$data->id)."'  class='btn btn-xs btn-white btn-info btn-bold edit-user' id='".$data->id."' >
        <i class='ace-icon fa fa-pencil bigger-120 blue'></i>View</a> |
        <a href='".route('customer.delete',$data->id)."' class='btn btn-white btn-warning btn-bold btn-xs delete_user' id='".$data->id."'>
        <i class='ace-icon fa fa-trash-o bigger-120 orange'></i> Delete</a>";
    })
    ->editColumn('status',function($data){
        if($data->status==0) return 'Inactive';
        if($data->status==1) return 'Active';
    })
    ->make(true);
}
}
