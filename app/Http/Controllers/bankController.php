<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\staffModel;
use App\bankModel;
use Yajra\Datatables\Datatables;

class bankController extends Controller
{
    public function index(){
    	$staffs=staffModel::all();
    	$trans=bankModel::all();
    	$deposit=bankModel::where('transaction','deposit')->sum('amount');
    	$withdraw=bankModel::where('transaction','withdraw')->sum('amount');
    	return view('bank.bankDetail',compact('staffs','trans','deposit','withdraw'));
    }
    public function depositDetails($id){
    	$staff=staffModel::find($id);
    	return view('bank.depositDetail',compact('staff'));
    }
    public function addTransaction($id){
    	$staff=staffModel::find($id);
    	return view('bank.addTransaction',compact('staff'));
    }
    public function saveTransaction(Request $request){
    	$this->validate($request,array(
    		'bank_name'=>'required',
    		'acc_no'=>'required',
    		'acc_name'=>'required',
    		'acc_type'=>'required',
    		'transaction'=>'required',
    		'transaction_by'=>'required',
    		'amount'=>'required',
    		'date'=>'required',
    	));
    	bankModel::create([
    		'bank_name'=>$request->bank_name,
    		'branch'=>$request->branch,
    		'acc_no'=>$request->acc_no,
    		'acc_name'=>$request->acc_name,
    		'acc_type'=>$request->acc_type,
    		'deposit_type'=>$request->deposit_type,
    		'transaction'=>$request->transaction,
    		'staff_id'=>$request->transaction_by,
    		'amount'=>$request->amount,
    		'remarks'=>$request->remarks,
    		'date'=>$request->date
    	]);
    	return redirect()->back();
    }
    public function staffsData(Datatables $datatables ,Request $request){
    	$trans=bankModel::where('staff_id',$request->name)->get();
    	return $datatables->of($trans)->make(true);
    }
    public function bankData(Datatables $datatables){
    	$trans=bankModel::with('staffs')->get();
    	return $datatables->of($trans)->make(true);
    }
}
