<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\staffModel;
use App\collectionModel;
use Session;
use Yajra\Datatables\Datatables;

class collectionController extends Controller
{
    public function index(){
    	$staffs=staffModel::all();
    	$debit=collectionModel::sum('dr_amt');
    	$credit=collectionModel::sum('cr_amt');
    	$total=$debit-$credit;
    	return view('collection.collection',compact('staffs','debit','credit','total'));
    }
    public function collectionDetails($id){
    	$staff=staffModel::find($id);
    	$uncollected=$staff->collections()->select('balance')->orderBy('id','desc')->first();
    	return view('collection.details',compact('staff','uncollected'));
    }
    public function postCollection($id){
    	$staff=staffModel::find($id);
    	$prev=$staff->collections()->select('balance')->orderBy('id','desc')->first();
    	return view('collection.postCollection',compact('staff','prev'));
    }
    public function saveCollection(Request $request){
    	$this->validate($request,array(
    		'date'=>'required',
    		'bill_from'=>'required',
    		'bill_to'=>'required',
    		'debit_amount'=>'required',
    	));
    	$result=collectionModel::create([
    		'date'=>$request->date,
    		'bill_from'=>$request->bill_from,
    		'bill_to'=>$request->bill_to,
    		'dr_amt'=>$request->debit_amount,
    		'cr_amt'=>$request->credit_amount,
    		'balance'=>$request->new_blnc,
    		'previous_collection'=>$request->prev_blnc,
    		'no_of_bill'=>$request->no_of_bill,
    		'staff_id'=>$request->staff_id,
    		'status'=>'active',
    	]);
    	if($result){
                Session::flash('success',' Inserted');
        
        }else{
             Session::flash('error','cannot  Inserted');
        }

    	return redirect()->back();
    }
    public function staffData(Datatables $datatables, Request $request){
    	$collections=collectionModel::where('staff_id',$request->name)->get();
   		return $datatables->of($collections)->make(true);
    }
}
