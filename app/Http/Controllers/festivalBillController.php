<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customerModel;
use App\festivalModel;
use App\staffModel;
use App\margaModel;
use App\reportModel;
use Session;
use Yajra\Datatables\Datatables;

class festivalBillController extends Controller
{
	public function postBill(){
		$customers=customerModel::all();
		return view('festivalBill.postBill',compact('customers'));
	}
	public function customerDetail($id){	
		$customer=customerModel::find($id);
		return view('festivalBill.customerDetail',compact('customer'));
	}
	public function saveBill(Request $request){
		$this->validate($request,array(
			'bill_no'=>'required|unique:tbl_festival,bill_no',
			'date'=>'required',
			'paid_amount'=>'required'
		));
		$result=festivalModel::create([
			'customer_id'=>$request->customer_id,
			'customer_code'=>$request->code_no,
			'customer_name'=>$request->customer_name,
			'marga_id'=>$request->marga_name,
			'staff_id'=>$request->staff_name,
			'bill_no'=>$request->bill_no,
			'date'=>$request->date,
			'paid_amount'=>$request->paid_amount,
			'remarks'=>$request->remarks,
		]);
		if($result){
            Session::flash('success','Bill Recorded');
            if(reportModel::where('customer_id',$request->customer_id)->count()<=0){
                reportModel::create([
                    'customer_code'=>$request->code_no,
                    'customer_id'=>$request->customer_id,
                    'customer_name'=>$request->customer_name,
                    'marga_id'=>$request->marga_name,
                    'staff_id'=>$request->staff_name
                ]);
            }
            reportModel::where('customer_id',$request->customer_id)->update([
            	'festival_bill'=>$request->bill_no,
            	'festival_date'=>$request->date,
            	'festival_amount'=>$request->paid_amount,
            ]);

        }else{
        	Session::flash('error','cannot  Inserted');
     	}
		return redirect()->back();
	}
	public function index(){
    	$festivals=festivalModel::all();
        $staffs=staffModel::all();
        $margas=margaModel::all();
    	return view('festivalBill.viewBill',compact('festivals','staffs','margas'));
    }
    public function getData(Datatables $datatables,Request $request){
        $query=festivalModel::with('staffs','margas')
        ->where(function($query)use($request){
        	$query->where('bill_no','like','%'.$request->b_no.'%')
        	->where('customer_name','like','%'.$request->c_name.'%');
        })

        ->get();
        return $datatables->of($query)->make(true);
    }
}
