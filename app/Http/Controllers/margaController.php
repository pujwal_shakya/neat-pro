<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\margaModel;
use Illuminate\Validation\Rule;
use Validator;
use Session;


class margaController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');

   }
   public function marga(){
    $margas=margaModel::all();
    return view('marga.marga',compact('margas'));
}
    public function addMarga(Request $request){
        $savedName = [];
        $notSavedName = [];
        $marga = margaModel::whereIn('name',$request->marga_name)->get(['name']);
        $input = array_unique(array_map('strToLower',$request->marga_name));
        foreach($input as $name){
            if (!empty($name)) {
                $checkMarga = $marga->filter(function($val) use ($name) {
                   return strtolower($name) == strtolower($val['name']);
               })->all();
                if(count($checkMarga)){
                    $notSavedName[] = $name;
                }else{
                    $savedName[] = ['name'=>$name,'staff_id'=>0];
                }
            }
        }
        if(count($savedName)){
            $result= margaModel::insert($savedName);
            Session::flash('success','New Marga Added');
        }
        if(count($notSavedName)){
            return redirect()->back()->with('error',implode(',',$notSavedName).' is already exist.');
        }
        return redirect()->back();
// $notSavedName = [];
// $this->validate($request,[
//     'marga_name.*'=>'unique:marga,name'
// ]);
// foreach ($request->marga_name as $key => $name) {
//     $marga = margaModel::where('name',$name)->get(['name']);
//     if(count($marga)){
//         $notSavedName[] = $name;
//     }else{

//     }
// }
// if(count($notSavedName)){
//     return redirect()->back()->with('error',implode(',',$notSavedName).' is already exist.');
// }
// return redirect()->back();
    }


    public function updateMarga(Request $request){

        $this->validate($request,[
            'name'=>['required',Rule::unique('marga')->ignore($request->id),],
        ]);
        margaModel::find($request->id)->update(['name'=>$request->name]);
    }
    public function deleteMarga($id){
        $marga=margaModel::find($id);
        $marga->customers()->update(['marga_id'=>0]);
        $marga->delete();
    }
}
