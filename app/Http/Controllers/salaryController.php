<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\salaryModel;
use App\staffModel;
use App\advSalaryModel;
use Yajra\Datatables\Datatables;

class salaryController extends Controller
{
    public function index(){
        $salaries=salaryModel::all();
    	return view('salary.salary',compact('salaries'));
    }
    public function addSalary(){
    	$staffs=staffModel::all();
    	$salaries=salaryModel::all();
    	return view('salary.addSalary',compact('staffs','salaries'));
    }
    public function staffDetail($id){
    	$staff=staffModel::find($id);
    	return view('salary.staffDetail',compact('staff'));
    }
    public function storeSalary(Request $request){
    	$this->validate($request,array(
    		'staff_id'=>'required',
    		'staff_name'=>'required',
    		'staff_desig'=>'required',
    		'salary'=>'required',
    		'date'=>'required',
    		'paid_up'=>'required',
    		'grand_total'=>'required'
    	));
    	$result=salaryModel::create([
    		'staff_id'=>$request->staff_id,
    		'staff_name'=>$request->staff_name,
    		'designation'=>$request->staff_desig,
            'm_status'=>$request->m_status,
            's_type'=>$request->s_type,
    		'salary'=>$request->salary,
    		'performance_allowance'=>$request->per_allo,
    		'expensive_allowance'=>$request->exp_allo,
    		'medical_allowance'=>$request->med_allo,
    		'communication_allowance'=>$request->comm_allo,
    		'waste_mgmt'=>$request->wst_allo,
    		'advance'=>$request->advance,
    		'pro_fund'=>$request->pf_amt,
    		'less_pf'=>$request->les_pf,
    		'tax'=>$request->tax_amt,
    		'paid_up_salary'=>$request->paid_up,
    		'date'=>$request->date,
    		'bonus_amount'=>$request->bonus,
    		'comission_amount'=>$request->comission,
    		'leave_amount'=>$request->less_leave,
    		'food_amount'=>$request->less_fooding,
    		'overtime_amount'=>$request->over_time,
            'additional_deduct'=>$request->addit_deduct,
    		'grand_total'=>$request->grand_total,
    		'remarks'=>$request->remarks,
    	]);
    	if($result){
            advSalaryModel::where('staff_id',$request->staff_id)->update(['status'=>'0']);
    		session::flash('success','Salary Added');
		}
		else{
    		session::flash('error','Salary Cannot Be Added');
		}
    	return redirect()->back();
    }
    public function editSalary($id){
        $salary=salaryModel::find($id);
        return view('salary.editSalary',compact('salary'));
    }

    public function advSalary(){
        $advances=advSalaryModel::all();
    	return view('salary.advSalary',compact('advances'));
    }
    public function addAdvSalary(){
    	$staffs=staffModel::all();
    	return view('salary.addAdvSalary',compact('staffs'));
    }
    public function AdvStaffDetail($id){
    	$staff=staffModel::find($id);
    	return view('salary.advSalaryForm',compact('staff'));
    }
    public function storeAdvSalary(Request $request){
    	$this->validate($request,array(
    		'staff_id'=>'required',
    		'basic_salary'=>'required',
    		'date'=>'required',
    		'adv_amount'=>'required'
    	));
    	$result=advSalaryModel::create([
    		'staff_id'=>$request->staff_id,
    		'basic_salary'=>$request->basic_salary,
    		'date'=>$request->date,
    		'amount'=>$request->adv_amount,
            'status'=>'1',
    		'remarks'=>$request->remarks
    	]);
    	if($result){
    		session::flash('success','Advanced Salary Added');
		}
		else{
    		session::flash('error','Advance Salary Cannot Be Added');
		}
    	return redirect()->back();
    }
    public function deleteAdvSalary($id){
        $advance=advSalaryModel::find($id);
        $advance->delete();
    }
    public function editAdvSalary($id){
        $advance=advSalaryModel::find($id);
        return view('salary.editAdvSalary',compact('advance'));
    }
    public function updateAdvSalary(Request $request,$id){
        $result=advSalaryModel::find($id)->update([
            'basic_salary'=>$request->basic_salary,
            'date'=>$request->date,
            'amount'=>$request->adv_amount,
            'remarks'=>$request->remarks
        ]);
        if($result){
            session::flash('success','Advanced Salary Updated');
        }
        else{
            session::flash('error','Advance Salary Cannot Be Updated');
        }
        return redirect()->back();
    }
    public function nameSearch(Request $request){
        return salaryModel::where('staff_name',$request->name)
        ->orwhere('staff_name','like','%'.$request->name.'%')->get();
    }
    public function data(Datatables $datatables,Request $request){
        $query=salaryModel::where(function($query)use($request){
            $query->where('staff_name','like','%'.$request->name.'%')
            ->where('staff_id','like','%'.$request->id.'%');
        })
        ->get();
        return $datatables->of($query)->make(true);
    }
}
