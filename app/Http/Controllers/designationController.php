<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Session;
use App\designationModel;
use App\staffModel;

class designationController extends Controller
{
  public function index(){
    $designations=designationModel::all();
    $staffs=staffModel::all();
    return view('designation',compact('designations','staffs'));
  }
  public function addDesignation(Request $request){
   $saveName=[];
   $unsaveName=[];
   $designation=designationModel::whereIn('designation',$request->designation_name)->get(['designation']);
   $input=array_unique(array_map('strtolower',$request->designation_name));
   foreach($input as $key=>$name){
    if(!empty($name)){
     $checkdesignation=$designation->filter(function($val)use($name){
      return strtolower($name) == strtolower($val['designation']);
    })->all();
     if(count($checkdesignation)){
      $unsaveName[]=$name;
    }
    else{
      if(isset($request->status[$key]) and !empty($request->status[$key])){
        $status=1;
      }
      else{
        $status=0;
      }
      $saveName[]=['designation'=>$name,'status'=>$status];
    }
  }
}
if(count($saveName)){
  $result=designationModel::insert($saveName);
  Session::flash('session','New Designation Added');
}
if(count($unsaveName)){
  return redirect()->back()->with('error',implode(',',$unsaveName).' is already exist');
}
return redirect()->back();
}
public function updateDesignation(Request $request){
  $this->validate($request,[
    'designation'=>['required',Rule::unique('tbl_designation')->ignore($request->id),],
  ]);
  designationModel::find($request->id)->update(['designation'=>$request->designation]);
}

public function deleteDesignation($id){
  $designation=designationModel::find($id);
  if(!count($designation->staffs)){
    $designation->delete();
  }
}   
}
