<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customerModel;
use App\billModel;
use App\staffModel;
use App\margaModel;
use App\reportModel;
use Yajra\Datatables\Datatables;
use Session;

class billController extends Controller
{
    public function postBill(){
    	$customers=customerModel::all();
    	return view('bill.postbill',compact('customers'));
    }
    public function customerDetail($id){	
    	$customer=customerModel::find($id);
    	return view('bill.customerDetail',compact('customer'));
    }
    public function saveBill(Request $request){
        $this->validate($request,array(
           'paid_amount'=>'required',
            'bill_no'=>'required|unique:tbl_bill,bill_no',
            'date'=>'required',
            'paid_from'=>'required',    
        ));
    	$result=billModel::create([
    		'customer_id'=>$request->customer_id,
    		'customer_code'=>$request->code_no,
    		'customer_name'=>$request->customer_name,
    		'marga_id'=>$request->marga_name,
    		'staff_id'=>$request->staff_name,
    		'bill_no'=>$request->bill_no,
    		'date'=>$request->date,
    		'paid_from'=>$request->paid_from,
    		'paid_upto'=>$request->paid_upto,
    		'monthly_rate'=>$request->monthly_charge,
    		'paid_amount'=>$request->paid_amount,
    		'member_fee'=>$request->member_fee,
    		'card_fee'=>$request->card_fee,
    		'drain_clean_fee'=>$request->drain_clean,
    		'late_fee'=>$request->late_fee,
    		'other_fee'=>$request->others,
    		'service_charge'=>$request->service_charge,
    		'less_discount'=>$request->less_discount,
    		'total'=>$request->grand_total,
    		'remarks'=>$request->remarks,
    	]);
        if($result){
            Session::flash('success','Bill Recorded');
            if(reportModel::where('customer_id',$request->customer_id)->count()<=0){
                reportModel::create([
                    'customer_code'=>$request->code_no,
                    'customer_id'=>$request->customer_id,
                    'customer_name'=>$request->customer_name,
                    'marga_id'=>$request->marga_name,
                    'staff_id'=>$request->staff_name
                ]);
            }
            if($request->paid_from >= $request->paid_upto){
                $i=$request->paid_upto;
                $j=$request->paid_from;
            }
            else{
                $i=$request->paid_from;
                $j=$request->paid_upto;
            }
            for($i;$i<=$j;$i++){
                reportModel::where('customer_id',$request->customer_id)->update([
                    'bill_'.$i=>$request->bill_no,
                    'date_'.$i=>$request->date,
                    'amount_'.$i=>$request->monthly_charge
                ]);
            }

        }else{
            Session::flash('error','cannot  Inserted');
        }
    	return redirect()->back();
    }
    public function index(){
    	$bills=billModel::all();
        $staffs=staffModel::all();
        $margas=margaModel::all();
    	return view('bill.viewBill',compact('bills','staffs','margas'));
    }
    // public function searchByName(Request $request){
    //      $data= billModel::where('customer_name',$request->name)
    //     ->orwhere('customer_name','like','%'.$request->name.'%')->get();
    // }
    public function getData(Datatables $datatables,Request $request){
        $query=billModel::with('staffs','margas')
        ->when($request->bill_no,function($query)use($request){
            $query->where('bill_no','like','%'.$request->bill_no.'%');
        })
        ->when($request->code_no,function($query)use($request){
            $query->where('customer_code','like','%'.$request->code_no.'%');
        })
        ->where('customer_name','like','%'.$request->name.'%')
        ->when($request->staff,function($query)use($request){
            $query->where('staff_id',$request->staff);
        })
        ->when($request->marga,function($query)use($request){
            $query->where('marga_id',$request->marga);
        })
        ->get();
        return $datatables->of($query)
        ->editColumn('paid_from',function($data){
            if($data->paid_from==1) return 'Baisakh';
            if($data->paid_from==2) return 'Jesth';
            if($data->paid_from==3) return 'Ashad';
            if($data->paid_from==4) return 'Shrawan';
            if($data->paid_from==5) return 'Bhadau';
            if($data->paid_from==6) return 'Aswin';
            if($data->paid_from==7) return 'Kartik';
            if($data->paid_from==8) return 'Mangsir';
            if($data->paid_from==9) return 'Poush';
            if($data->paid_from==10) return 'Magh';
            if($data->paid_from==11) return 'Falgun';
            if($data->paid_from==12) return 'Chaitra';
        })
        ->editColumn('paid_upto',function($data){
            if($data->paid_upto==1) return 'Baisakh';
            if($data->paid_upto==2) return 'Jesth';
            if($data->paid_upto==3) return 'Ashad';
            if($data->paid_upto==4) return 'Shrawan';
            if($data->paid_upto==5) return 'Bhadau';
            if($data->paid_upto==6) return 'Aswin';
            if($data->paid_upto==7) return 'Kartik';
            if($data->paid_upto==8) return 'Mangsir';
            if($data->paid_upto==9) return 'Poush';
            if($data->paid_upto==10) return 'Magh';
            if($data->paid_upto==11) return 'Falgun';
            if($data->paid_upto==12) return 'Chaitra';
        })
        ->make(true);
    }
}
