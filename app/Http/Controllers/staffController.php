<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\staffModel;
use App\margaModel;
use App\designationModel;
use Image;
use Session;


class staffController extends Controller
{
    public function __construct()
    {
     $this->middleware('auth');

 }
 public function staff(){
    $staffs = staffModel::all();
    $designations=designationModel::all();
    return view('staff.staff',compact('staffs','designations'));
}
public function addStaff(){
    $designations=designationModel::all();
   return view('staff.addStaff',compact('designations'));
}
public function saveStaff(Request $request){
   $this->validate($request,array(
    'name'=>'required|max:25',
    'gender'=>'required',
    'm_status'=>'required',
    's_type'=>'required',
    'phone'=>'required|max:10',
    'address'=>'required|max:25',
    'degsination_id'=>'required',
    'salary'=>'required'
));
        // if($request->hasFile('image')){
    	// 	$image=$request->file('image');
    	// 	$filename=time().'.'.$image->getClientOriginalExtension();
    	// 	$location=public_path('images/'.$filename);
    	// 	Image::make($image)->resize(800,800)->save($location);
    	// }
    	// else{
    	// 	$filename=null;

   if($request->hasFile('image')){
    $image=$request->file('image');
    $destinationPath=public_path().'/uploads/';
    $filename=time(). '.' . $image->getClientOriginalExtension();
    $image->move($destinationPath,$filename);
}
else{
    $filename=null;
}
$result=staffModel::create([
    'name'=>$request->name,
    'gender'=>$request->gender,
    'm_status'=>$request->m_status,
    's_type'=>$request->s_type,
    'phone'=>$request->phone,
    'address'=>$request->address,
    'designation_id'=>$request->degsination_id,
    'salary'=>$request->salary,
    'performance_allowance'=>$request->per_allowance,
    'expensive_allowance'=>$request->exp_allowance,
    'medical_allowance'=>$request->med_allowance,
    'communication_allowance'=>$request->com_allowance,
    'waste_mgmt'=>$request->waste_mgmt,
    'image'=>$filename]);
if($result){
    session::flash('success','New Staff Has Been Added');
}
else{
    session::flash('error','Staff Cannot Be Added');
}
return redirect()->back();
}

public function collector($id){
   $staffs=staffModel::find($id);
   $margas=margaModel::where('staff_id',0)->get();
   if(!empty($staffs)){
    return view('staff.collector',compact('staffs','margas'));
}
else{
    return view('errors.404');
}
}

public function assignMarga(Request $request, $id){
    $marga_id=$request->chk;
    foreach($marga_id as $m_id){
        margaModel::find($m_id)->update(['staff_id'=>$id]);
    }
    return redirect()->back();
}
public function assignNewMarga(Request $request){
    $this->validate($request,array(
        'name'=>'required|unique:marga,name',
    ));
    $marga = margaModel::create(['name'=>$request->name,'staff_id'=>0]);
    return margaModel::find($marga->id);
}
public function removeMarga($id){
    $marga = margaModel::find($id);
    $marga->update(['staff_id'=>0]);
    return response($marga);
}
public function staffDetail($id){
    $staffs=staffModel::find($id);
    $designations=designationModel::all();
    if(!empty($staffs)){
        return view('staff.staffDetail',compact('staffs','designations'));
    }else{
        return view('errors.500');
    }
}
public function updateDetail($id, Request $request){
    $this->validate($request,array(
        'name'=>'required|max:25',
        'gender'=>'required',
        'phone'=>'required|max:10',
        'address'=>'required|max:25',
        'designation_id'=>'required',
        'salary'=>'required'
    ));
    $result=staffModel::find($id)->update([
        'name'=>$request->name,
        'gender'=>$request->gender,
        'm_status'=>$request->m_status,
        's_type'=>$request->s_type,
        'phone'=>$request->phone,
        'address'=>$request->address,
        'designation_id'=>$request->designation_id,
        'salary'=>$request->salary,
        'performance_allowance'=>$request->per_allowance,
        'expensive_allowance'=>$request->exp_allowance,
        'medical_allowance'=>$request->med_allowance,
        'communication_allowance'=>$request->com_allowance,
        'waste_mgmt'=>$request->waste_mgmt
    ]);
    if($result){
        Session::flash('success','Detail Has Been Updated');
    }
    else{
        Session::flash('error','Detail Cannot Be Updated');
    }
    return redirect()->back();
}
public function deleteStaff($id){
    $staff=staffModel::find($id);
    if(!empty($staff['image'])){
        unlink(public_path('uploads/'.$staff['image']));
    }
    $staff->margas()->update(['staff_id'=>0]);
    $staff->delete();
    return redirect()->back();
}
}
