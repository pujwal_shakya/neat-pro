<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\reportModel;
use App\margaModel;
use Illuminate\Support\Facades\DB;

class reportController extends Controller
{
    public function index(){
    	return view('report.customerReport');
    }
    public function dueReport(){
    	$reports=reportModel::all();
    	return view('report.dueReport');
    }
    public function getData(Request $request){
    	$data=reportModel::where(function($query)use($request){
    		$query->where('customer_code',$request->code_no)
    		->whereYear('created_at','=',date($request->year));
    	})->first();
    	if($data){
    		return view('report.dueDetail',compact('data'));
    	}
    	else{
    		return 'Data Record Found In Database';
    	}
    }
    public function annualReport(){
    	$reports=reportModel::all();
    	return view('report.annualReport');
    }
    public function annualData(Request $request){
    	$datas=reportModel::whereYear('created_at','=',date($request->date))->get();
    	return view('report.annualDetail',compact('datas'));

    }
    public function collectionReport(){
    	$margas=margaModel::all();
    	return view('report.collectionReport',compact('margas'));
    }
}
