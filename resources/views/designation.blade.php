@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="">Add Designation</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Succesfull !! &nbsp;</strong>{{Session::get('success')}}
    </div>
    @endif
    @if(Session::get('error'))
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Sorry !! &nbsp;</strong>{{Session::get('error')}}
    </div>
    @endif
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
            <div class="well">
             <button role="button" class="btn btn-success" data-toggle="modal" data-target="#designation-modal">
                  <i class="fa fa-plus"></i>
                  Add Designation
              </button>
              <div class="pull-right">
                  <h4 class="label label-xlg label-white middle">Total Designation: {{$designations->count()}}</h4>
              </div>
              <div class="hr hr-double hr-dotted hr18"></div>
              <div class="table-responsive" id="list-staff">
                <!-- <div class="well">{{implode(',',$errors->all())}}</div> -->
                <table class="table table-striped table-bordered table-hover table-resposnive" id="marga">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Designation Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($designations as $key=>$designation)
                        <tr id='{{$designation->id}}'>
                            <td>{{$key+1}}</td>
                            <td>{{ucFirst($designation->designation)}}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="edit-faculty btn btn-xs btn-warning" onclick="Edit({{$designation}})">
                                        <span class="ace-icon fa fa-edit bigger-120"></span>
                                        Edit Name
                                    </a>
                                    &nbsp; &nbsp;
                                    @if(!count($designation->staffs))
                                    <a role="button" class="btn btn-xs btn-danger remove_marga" onclick="Delete({{$designation->id}})" id="{{$designation->id}}">
                                        <span class="ace-icon fa fa-trash-o bigger-120"></span>
                                        Remove
                                    </a>
                                    @else
                                    <span class="ace-icon fa fa-close bigger-120"></span>
                                    <span class="ace-icon fa fa-close bigger-120"></span>
                                    @endif
                                </div>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="designation-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{asset('designation/save')}}" id="modalform" method="post" class="form-horizontal clearfix" role="form" >
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Designation</h4>
                </div>
                <div class="widget-box">
                    <table class="table"  id="add_designation_table">
                        <tbody>

                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" placeholder="Designation Name" name="designation_name[]"  required/>
                                            <div class="checkbox">
                                                <label>
                                                    <input  class="ace ace-checkbox" type="checkbox"
                                                    name="status[0]"  value="1">
                                                    <span class="lbl">Assign Marga</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-md-8">
                                        <a role="button" id="addbutton" class="btn btn-success btn-app  btn-xs" >
                                            <i class=" fa fa-plus"></i></a>                                       
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-sm btn-primary" id="designation_submit">
                            <i class="ace-icon fa fa-check" ></i>
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Designation</h4>
                </div>
                <div class="widget-box">
                    <table class="table"  id="add_designation_table">
                        <tbody>

                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="Designation Name" name="designation_name" id="designation_name" required/>
                                            <input type="hidden" name="desig_id" id="designation_id">
                                            <div id="margaerror" style="color:red"></div>
                                            <!-- <input type="radio" name="status" value="1" class="ace" id="status1">
                                            <label class="lbl green " for=status1>
                                                Allocation of Marga Needed
                                            </label>
                                            &nbsp;&nbsp;
                                            <input type="radio" name="status" value="0" class="ace" checked="" id="status0">
                                            <label class="lbl text-danger" for="status0">
                                                Allocation Not Needed
                                            </label> -->
                                        </div>
                                    </div>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Cancel
                        </button>
                        <button role="button" class="btn btn-sm btn-primary" id="designation_edit">
                            <i class="ace-icon fa fa-check" ></i>
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script>
    var i=1;
	$('#add_designation_table').on('click','#addbutton',function(){
		var row = '<tr>'+
                    '<td>'+
                        '<div class="form-group">'+
                            '<div class="col-md-12">'+
                                '<input type="text" class="form-control" placeholder="Designation Name" name="designation_name[]"  required/>'+
                                '<div class="checkbox">'+
                                    '<label>'+
                                        '<input  class="ace ace-checkbox" type="checkbox" name="status['+i+']" value="1">'+
                                       '<span class="lbl">Assign Marga</span>'+
                                    '</label>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        '<div class="col-md-8">'+
                            '<a role="button" id="removebutton" class="btn btn-danger btn-app  btn-xs" >'+
                            '<i class=" fa fa-minus"></i></a> '+                                     
                        '</div>'+
                    '</td>'+
                  '</tr>';
                  $('#add_designation_table').append(row);
                  i++;
	});
	$('#add_designation_table').on('click','#removebutton',function(){
		if(confirm('Are you sure want to delete?')==false){
			return false;
		}
		else{
			$(this).closest('tr').remove();
		}
	});
    function Edit(response){
        $('#designation_name').val(response.designation);
        $('#designation_id').val(response.id);
        $('#edit-modal').modal();
    }
    $('#designation_edit').on('click',function(e){
        e.preventDefault();
        var desig_name= $('#designation_name').val();
        var desig_id= $('#designation_id').val();
        var data={'designation':desig_name,'id':desig_id,'_token':'{{csrf_token()}}'}
        $.ajax({
            data:data,
            type:'post',
            url:"{{asset('designation/update')}}",
            success:function(response){
                 window.location.reload();
            },
            error: function ( jqXhr, json, errorThrown ) 
            {
                var errors = jqXhr.responseJSON;
                var errorsHtml= '';
                $.each( errors, function( key, value ) {
                    errorsHtml += value[0]; 
                });
                $("#margaerror").html(errorsHtml);
                $("#margaerror").addClass('has-error');
                $("#margaerror").fadeIn(300);
                $("#margaerror").delay(3200).fadeOut(300);
            },
        });
    })

    function Delete(id){
        if(confirm('Are you sure want to delete?')==false){
            return false;   
        }
        else{
            $.ajax({
                url:"{{asset('designation/remove')}}/"+id,
                type:'get',
                success:function(response){
                    $('#'+id).remove();
                }
            });
        }
    }
</script>

@endpush