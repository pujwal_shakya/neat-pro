@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('staff')}}">List Staff</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="well">
				<a href="{{asset('staff/add_staff')}}" class="btn btn-success">
					<i class="fa fa-plus"></i>Add New Staff
				</a>
				<div class="pull-right">
					<h4 class="label label-xlg label-white middle">Total Staffs: {{$staffs->count()}}</h4>
				</div>
				<div class="hr hr-double hr-dotted hr18"></div>
				<div class="table-responsive" id="list-staff">
					<table class="table table-striped table-bordered table-hover table-resposnive" id="staff">
						<thead>
							<tr>
								<th>S.N</th>
								<th>Name</th>
								<th>Staff Id</th>
								<th>Gender</th>
								<th>Marital Status</th>
								<th>Phone</th>
								<th>Address</th>
								<th>Designtion</th>
								<th>Staff Type</th>
								<th>Basic Salary</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($staffs as $key=>$staff)
							<tr id="{{$staff->id}}" >
								<td>{{$key+1}}</td>
								<td>{{ucFirst($staff->name)}}</td>
								<td>{{$staff->id}}</td>
								<td>{{$staff->gender}}</td>
								<td>{{$staff->m_status}}</td>
								<td>{{$staff->phone}}</td>
								<td>{{$staff->address}}</td>
								<td>{{$staff->designations['designation']}}</td>
								<td>
									@if($staff->s_type=='full')
									Full Time
									@elseif($staff->s_type=='part')
									Part Time
									@elseif($staff->s_type=='cont')
									Contracted
									@endif
								</td>
								<td>{{$staff->salary}}</td>
								<td>
									<div class=" btn-group">
										@if($staff->designations['status']==1)
										<a href="staff/add_collector/{{$staff->id}}" class="assign-tol btn btn-xs btn-primary">
											<span class="glyphicon glyphicon-plus"></span>
											Collector
										</a>
										@endif
										<a href="staff/detail/{{$staff->id}}" class="edit-faculty btn btn-xs btn-warning">
											<span class="glyphicon glyphicon-eye-open"></span>
											View Detail
										</a>
										<a role="button" class="btn btn-xs btn-danger remove_marga" 
										onclick="Remove({{$staff->id}})" id="{{$staff->id}}">
										<span class="ace-icon fa fa-trash-o"></span>
										Remove
									</a>
								</div>
							</th>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
</div>
@endsection

@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script>
	function Remove(id){
		if(confirm('Are You Sure Want To Remove?')==false){
			return false;
		}else{
			$.ajax({
				url:"{{asset('staff/remove')}}/"+id,
				type:'get',
				success:function(response){
					$('#'+id).remove();
				}
			});
		}
	}
	$(document).ready(function(){
		$('#staff').DataTable();
	});
</script>

@endpush