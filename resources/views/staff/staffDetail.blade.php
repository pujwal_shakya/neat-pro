@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('staff')}}">List Staff</a>
		</li>
		<li>
			<a href="">Edit Detail</a>
		</li>
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Succesfull !! &nbsp;</strong>{{Session::get('success')}}
	</div>
	@endif
	@if(Session::get('error'))
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Sorry !! &nbsp;</strong>{{Session::get('error')}}
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="well">
				<div class="table-header">
					Details Of {{$staffs->name}}
				</div>
				<div class="space space-8"></div>
				<form action="{{asset('staff/editdetail')}}/{{$staffs->id}}" method="POST" role='form' class="form-horizontal" enctype="multipath/form-data">
					{{csrf_field()}}
					<fieldset>
						<div class="form-group">
							<label class="control-label col-md-2" for="name">
								Name
							</label>
							<div class="col-md-10">
								<input type="text" autocomplete="off" id="name" name="name" value="{{$staffs->name}}" class="form-control" placeholder="enter full name here...">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="phone">
								Marital Status
							</label>
							<div class="col-md-4">
								<input type="radio" name="m_status" value="married" class="ace">
								<span class="lbl" for="m_s">
									Married
								</span>
								&nbsp;&nbsp;
								<input type="radio" name="m_status" value="unmarried" class="ace" checked>
								<span class="lbl">
									Unmarried
								</span>
							</div>
							<label class="control-label col-md-2" for="address">
								Staff Type
							</label>
							<div class="col-md-4">
								<select name="s_type" id="s_type" class="form-control" required>
									<option value="full">Full Time Staff</option>
									<option value="part">Part Time Staff</option>
									<option value="cont">Contracted Staff</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="phone">
								Phone
							</label>
							<div class="col-md-4">
								<input type="number" name="phone" id="phone" autocomplete="off" value="{{$staffs->phone}}" class="form-control" placeholder="enter phone number here...">	
							</div>
							<label class="control-label col-md-2" for="address">
								Address
							</label>
							<div class="col-md-4">
								<input type="text" autocomplete="off" id="address" value="{{$staffs->address}}" name="address" class="form-control" placeholder="enter address here...">
							</div>
						</div>
						<input type="hidden" name="gender" value="{{$staffs->gender}}">
						
						<div class="form-group">
							<label class="control-label col-md-2">
								Degsination
							</label>
							<div class="col-md-4">
								<select name="designation_id" id="designation_id" class="form-control">
									@foreach($designations as $designation)
										<option value="{{$designation->id}}">{{$designation->designation}}
										</option>
									@endforeach
								</select>
							</div>
							<label class="control-label col-md-2" for="salary">
								Basic Salary
							</label>
							<div class="col-md-4">
								<input type="number" name="salary" id="salary" autocomplete="off" value="{{$staffs->salary}}" class="form-control" placeholder="enter salary here...">
							</div>	
							
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Performance Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="per_allowance" value="{{$staffs->performance_allowance}}" id="per_allowance" autocomplete="off" class="form-control" placeholder="enter Performance Allowance here...">
							</div>
							<label class="control-label col-md-2" for="exp_allowance">
								Expensive Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="exp_allowance" value="{{$staffs->expensive_allowance}}" id="exp_allowance" autocomplete="off" class="form-control" placeholder="enter Expensive Allowance here...">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="med_allowance">
								Medical Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="med_allowance" value="{{$staffs->medical_allowance}}" id="med_allowance" autocomplete="off" class="form-control" placeholder="enter Medical Allowance here...">
							</div>
							<label class="control-label col-md-2" for="com_allowance">
								Communication Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="com_allowance" value="{{$staffs->communication_allowance}}" id="com_allowance" autocomplete="off" class="form-control" placeholder="enter Communication Allowance here...">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="waste_mgmt">
								Waste Management
							</label>
							<div class="col-md-4">
								<input type="number" name="waste_mgmt" value="{{$staffs->waste_mgmt}}" id="waste_mgmt" autocomplete="off" class="form-control" placeholder="enter Waste Management here...">
							</div>
							<hr>
							<hr>
							<div class="col-md-8 col-md-offset-4" >
								<div class="col-md-4">
									<input type="submit" name="save_staff" value="Save Change" class="btn btn-success col-md-12" >
								</div>
								<div class="col-md-4">
									<a href="{{asset('staff')}}" class="btn btn-danger col-md-12">Cancel</a>
								</div>
							</div>
						</div>
					</fieldset>
					
				</form>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>
@endsection