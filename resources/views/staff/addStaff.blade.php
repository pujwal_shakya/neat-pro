@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('staff')}}">List Staff</a>
		</li>
		<li>
			<a href="{{asset('staff/add_staff')}}">Create New Staff</a>
		</li>
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Succesfull !! &nbsp;</strong>{{Session::get('success')}}
	</div>
	@endif
	@if(Session::get('error'))
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Sorry !! &nbsp;</strong>{{Session::get('error')}}
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="well">
				<div class="table-header">
					Donates Required Field
				</div>
				<div class="space space-8"></div>
				<form action="{{asset('staff/save_staff')}}" method="POST" role='form' class="form-horizontal" enctype="multipart/form-data" id="add_staff">
					{{csrf_field()}}
					<fieldset>
						<div class="form-group">
							<label class="control-label col-md-2" for="name">
								Name
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-10">
								<input type="text" autocomplete="off" id="name" name="name" value="{{old('name')}}" class="form-control" placeholder="enter full name here..." required>
								<span class="text-danger">{{$errors->first('name')}}</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="phone">
								Phone
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="number" name="phone" value="{{old('phone')}}" id="phone" autocomplete="off" class="form-control" placeholder="enter phone number here..." required>	
								<span class="text-danger">{{$errors->first('phone')}}</span>
							</div>
							<label class="control-label col-md-2" for="address">
								Address
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="text" autocomplete="off" id="address" name="address" value="{{old('address')}}" class="form-control" placeholder="enter address here..." required>
								<span class="text-danger">{{$errors->first('address')}}</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Gender
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="radio" name="gender" value="male" class="ace" checked>
								<span class="lbl">
									Male
								</span>
								&nbsp;&nbsp;
								<input type="radio" name="gender" value="female" class="ace" >
								<span class="lbl">
									Female
								</span>
								<span class="text-danger">{{$errors->first('gender')}}</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Marital Status
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="radio" name="m_status" value="married" class="ace">
								<span class="lbl" for="m_s">
									Married
								</span>
								&nbsp;&nbsp;
								<input type="radio" name="m_status" value="unmarried" class="ace" checked>
								<span class="lbl">
									Unmarried
								</span>
								<span class="text-danger">{{$errors->first('gender')}}</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Staff Type
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<select name="s_type" id="s_type" class="form-control" required>
									<option value="full">Full Time Staff</option>
									<option value="part">Part Time Staff</option>
									<option value="cont">Contracted Staff</option>
									<!-- <option value="collector">Collector</option>
									<option value="supervisor">Supervisor</option> -->
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Degsination
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<select name="degsination_id" id="degsination" class="form-control" required>
									@foreach($designations as $desig)
									<option value="{{$desig->id}}">{{$desig->designation}}</option>

									@endforeach
									<!-- <option value="collector">Collector</option>
									<option value="supervisor">Supervisor</option> -->
								</select>
								<span class="text-danger">{{$errors->first('degsination')}}</span>
							</div>
							<label class="control-label col-md-2" for="salary">
								Basic Salary
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="number" name="salary" value="{{old('salary')}}" id="salary" autocomplete="off" class="form-control" placeholder="enter salary here..." required>
								<span class="text-danger">{{$errors->first('salary')}}</span>
							</div>	

						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Performance Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="per_allowance" value="{{old('per_allowance')}}" id="per_allowance" autocomplete="off" class="form-control" placeholder="enter Performance Allowance here...">
							</div>
							<label class="control-label col-md-2" for="exp_allowance">
								Expensive Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="exp_allowance" value="{{old('exp_allowance')}}" id="exp_allowance" autocomplete="off" class="form-control" placeholder="enter Expensive Allowance here...">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="med_allowance">
								Medical Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="med_allowance" value="{{old('med_allowance')}}" id="med_allowance" autocomplete="off" class="form-control" placeholder="enter Medical Allowance here...">
							</div>
							<label class="control-label col-md-2" for="com_allowance">
								Communication Allowance
							</label>
							<div class="col-md-4">
								<input type="number" name="com_allowance" value="{{old('com_allowance')}}" id="com_allowance" autocomplete="off" class="form-control" placeholder="enter Communication Allowance here...">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="waste_mgmt">
								Waste Management
							</label>
							<div class="col-md-4">
								<input type="number" name="waste_mgmt" value="{{old('waste_mgmt')}}" id="waste_mgmt" autocomplete="off" class="form-control" placeholder="enter Waste Management here...">
							</div>
							<label class="control-label col-md-2">
								Staff Image
							</label>
							<div class="col-md-4">
								<input type="file" name="image" value="{{old('image')}}" id="image">
							</div>
							<hr>
							<hr>
							<div class="col-md-8 col-md-offset-4" >
								<div class="col-md-4">
									<input type="submit" name="save_staff" value="Save" class="btn btn-success col-md-12" >
								</div>
								<div class="col-md-4">
									<a href="{{asset('staff')}}" class="btn btn-danger col-md-12">Cancel</a>
								</div>
							</div>
						</div>
					</fieldset>

				</form>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>
@endsection
@push('script')
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
	$('#image').ace_file_input({
					no_file:'No File ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:false,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
	});

	$('#add_staff').validate({
	errorElement: 'div',
	errorClass: 'help-block',
	highlight: function (e) {
		$(e).closest('div').removeClass('has-info').addClass('has-error');
	},
});
</script>
@endpush