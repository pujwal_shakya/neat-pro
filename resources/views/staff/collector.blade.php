@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('staff')}}">List Staff</a>
		</li>
		<li>
			<a href="">Collector</a>
		</li>
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- Page Content begins -->
			<div class="col-md-12">
				<div class="widget-box">
					<div class="widget-header">
						<h5 class="widget-title bigger lighter">Collector : {{$staffs->name}}</h5>
					</div>
                </div>
                <div class="pull-right">
                 <button role="button" class="btn btn-xs btn-warning" onclick="collector({{$staffs}})">
                  <i class="fa fa-plus"></i>Assign New
              </button>
          </div>
          <div class="clearfix"></div>
          <div class="space-2"></div>
          <div class="table-responsive col-md-12" id="list-staff">
            <table class="table table-striped table-bordered table-hover table-resposnive" id="staff">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Marga Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($staffs->margas as $key=>$marga)
                    <tr id='{{$marga->id}}'>
                        <td>{{$key+1}}</td>
                        <td>{{$marga->name}}</td>
                        <td>
                            <div class="hidden-sm hidden-xs btn-group">
                                <a role="button" class="btn btn-xs btn-danger remove_marga" onclick="Remove({{$marga->id}})" id="{{$marga->id}}">
                                    <span class="ace-icon fa fa-trash-o bigger-120"></span>
                                    Remove
                                </a>
                            </div>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- Page Content Ends -->
</div>
</div>
</div>
<div id="collector-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="col-md-4">
                    <h4 class="modal-title">Select Marga</h4>
                </div>
                <div>
                    <input type="text" name="assignmarga" id="assignmarga" placeholder="Assing New Marga">
                    <button class="btn btn-sm btn-warning" id="assignbutton">Assign</button>
                    <p style="color:red" id="assignerror"></p>
                </div>
            </div>
            <form action="{{asset('staff/add_collector/assignmarga')}}/{{$staffs->id}}" id="modalform" method="post" class="form-horizontal clearfix" role="form" >
                {{csrf_field()}}
                <div class="modal-content">
                	<div class="widget-box">
                		<table class="table" id="add_marga">
                			<thead>
                				<tr>
                                    <th>Check Box</th>
                                    
                                    <th>Marga Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($margas as $marga)
                                <tr>
                                    <th>
                                        <div class="checkbox">
                                            <label>
                                                <input  class="ace ace-checkbox" type="checkbox"
                                                name="chk[]" id="chk1{{$marga->id}}" value="{{$marga->id}}">
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </th>
                                    
                                    <th>
                                        <label for="chk1{{$marga->id}}">{{$marga->name}}</label>               
                                        
                                    </th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-sm btn-primary" id="collector_submit">
                        <i class="ace-icon fa fa-check"></i>
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('script')
<script>
	function collector(response){
		$('#collector-modal').modal();

	}
    function Remove(id){
        $.ajax({
            url:"{{asset('staff/add_collector/removemarga')}}/"+id,
            type:'get',
            success:function(response){
               $('#'+id).remove();
               var rows= '<tr>'+
               '<label>'+
               '<td><div class="checkbox"><label><input  class="ace ace-checkbox" type="checkbox" name="chk[]" id="chk1'+response.id+'" value="'+response.id+'" > <span class="lbl"></span></label></div></td>'+'<td>'+response.name+'</td>'+
               '<tr>';
               $('#add_marga').append(rows);
           }
       });
    }

    $('#assignbutton').on('click',function(e){
        e.preventDefault();
        var assignmarga= $("#assignmarga").val();
        // var staff_id = "{{$staffs->id}}";
        $.ajax({
            type:'get',
            url:"{{asset('staff/assignNewMarga')}}?name="+assignmarga,
            success:function(response){
                var rows=   '<tr>'+
                '<label>'+
                '<td><div class="checkbox"><label><input  class="ace ace-checkbox" type="checkbox" name="chk[]" id="chk1'+response.id+'" value="'+response.id+'" checked> <span class="lbl"></span></label></div></td>'+
                '<td>'+response.name+'</td>'+
                '<tr>';
                $('#add_marga').append(rows);
            },
            error: function ( jqXhr, json, errorThrown ) 
           {
            var errors = jqXhr.responseJSON;
            var errorsHtml= '';
            $.each( errors, function( key, value ) {
                errorsHtml += value[0]; 
            });
            $("#assignerror").html(errorsHtml);
            $("#assignerror").addClass('has-error');
            $("#assignerror").fadeIn(300);
            $("#assignerror").delay(3200).fadeOut(300);
        },

        });
    })
    
</script>

@endpush