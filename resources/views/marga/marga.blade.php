@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Home</a>
        </li>

        <li>
            <a href="{{asset('marga')}}">Marga</a>
        </li>
        
    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
    @if(Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Succesfull !! &nbsp;</strong>{{Session::get('success')}}
    </div>
    @endif
    @if(Session::get('error'))
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Sorry !! &nbsp;</strong>{{Session::get('error')}}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <!-- <div >
                <p>
                    {{implode(',',$errors->all())}}
                    @if(session('error'))
                    {{session('error')}}
                    @endif
                </p>
            </div> -->
            <div class="well">
               <button role="button" class="btn btn-success" data-toggle="modal" data-target="#marga-modal">
                  <i class="fa fa-plus"></i>
                  Add New Marga
              </button>
              <!-- <button role="button" class="btn btn-info">
                  <i class="fa fa-print"></i>
                  Print Preview
              </button>
              <button role="button" class="btn btn-warning">
                  <i class=" fa fa-external-link"></i>
                  Export
              </button> -->
              <div class="pull-right">
                  <h4 class="label label-xlg label-white middle">Total Marga: {{$margas->count()}}</h4>
              </div>
              <div class="hr hr-double hr-dotted hr18"></div>
              <div class="table-responsive" id="list-staff">
                <!-- <div class="well">{{implode(',',$errors->all())}}</div> -->
                <table class="table table-striped table-bordered table-hover table-resposnive" id="marga">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Marga Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($margas as $key=>$marga)
                        <tr id='{{$marga->id}}'>
                            <td>{{$key+1}}</td>
                            <td>{{ucFirst($marga->name)}}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="edit-faculty btn btn-xs btn-warning" 
                                    onclick="EditMarga({{$marga}})">
                                        <span class="ace-icon fa fa-edit bigger-120"></span>
                                        Edit Name
                                    </a>
                                    <a role="button" class="btn btn-xs btn-danger remove_marga" onclick="Delete({{$marga->id}})" id="{{$marga->id}}">
                                        <span class="ace-icon fa fa-trash-o bigger-120"></span>
                                        Remove
                                    </a>
                                </div>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div>
<div id="marga-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{asset('marga/save')}}" id="modalform" method="post" class="form-horizontal clearfix" role="form" >
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Marga</h4>
                </div>
                <div class="widget-box">
                    <table class="table"  id="add_marga_table">
                        <tbody>

                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" placeholder="Marga Name" name="marga_name[]"  required/>
                                            <span class="text-danger">{{$errors->first('marga_name')}}</span>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-md-8">
                                        <a role="button" id="addbutton" class="btn btn-success btn-app  btn-xs" >
                                            <i class=" fa fa-plus"></i></a>                                       
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-sm btn-primary" id="marga_submit">
                            <i class="ace-icon fa fa-check" ></i>
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal starts -->
    <div id="edit-modal" class="modal fade"  tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Marga</h4>
                </div>
                <div class="widget-box light-blue">
                    <table class="table" id="add_marga">
                        <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Marga Name" id="marga_name" name="marga_name"  required/>
                                            <input type="hidden" name="marga_id" id="marga_id">
                                            <div id="margaerror" style="color:red"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Cancel
                    </button>
                    <button role="button" class="btn btn-sm btn-primary" id="marga_edit">
                        <i class="ace-icon fa fa-check"></i>
                        Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit Modal Ends -->

    @endsection
    @push('script')
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/dataTables.select.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script>

        @if(count($errors)>0)
        $('#marga-modal').modal('show');
        @endif

        function EditMarga(response){
            $('#marga_name').val(response.name);
            $('#marga_id').val(response.id);
            $('#edit-modal').modal();
        }

        $('#marga_edit').on('click',function(e){
    e.preventDefault();
    var marganame = $('#marga_name').val();
    var id = $('#marga_id').val() ;
    var data={'id':id,'name':marganame,'_token':'{{csrf_token()}}'};
    $.ajax({
        data:data,
        type:'post',
        url:"{{asset('marga/update')}}",
        success:function(response){
           window.location.reload();
       },
       error: function ( jqXhr, json, errorThrown ) 
       {
        var errors = jqXhr.responseJSON;
        var errorsHtml= '';
        $.each( errors, function( key, value ) {
            errorsHtml += value[0]; 
        });
        $("#margaerror").html(errorsHtml);
        $("#margaerror").addClass('has-error');
        $("#margaerror").fadeIn(300);
        $("#margaerror").delay(3200).fadeOut(300);
    },
});
})

function Delete(id){
    if(confirm('Are You Sure Want To Remove?')==false){
        return false;
    }else{

        $.ajax({
            url:"{{asset('marga/delete')}}/"+id,
            type:'get',
            success:function(response){
                $('#'+id).remove();
            }
        });
    }
}
$(document).ready(function(){
            $('#marga').DataTable();
        });

        $('#add_marga_table ').on('click','#addbutton',function(){          
         var row='<tr>'+
         '<td>'+
         '<div class="form-group">'+
         '<div class="col-md-12">'+
         '<input type="text" class="form-control" value="" placeholder="Marga Name" name="marga_name[]"  required/>'+
         '</div>'+
         '</div>'+
         '</td>'+
         '<td>'+
         '<div class="col-md-8">'+
         '<a role="button" class="removebutton btn btn-danger btn-app  btn-xs">'+
         '<i class=" fa fa-minus"></i></a>'+                                       
         '</div>'+
         '</td>'+
         '</tr>';
         $('#add_marga_table tbody').append(row);
     });

     $('#add_marga_table ').on('click','.removebutton',function(){
            if(confirm('Are you sure want to remove ?')==false){
                return false;
            }else{
              $(this).closest('tr').remove();
          }
      }); 

      $('#modalform').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
    });

    

</script>

@endpush