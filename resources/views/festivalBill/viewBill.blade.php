@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/nepali.datepicker.v2.2.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Home</a>
        </li>
        <li>
            <a href="">Festival Bill View</a>
        </li>
    </ul>
   <!-- /.breadcrumb -->
</div>
<div class="page-content">
  <div class="row">
    <div class="col-xs-12  well">
      <!-- <div>
        <a href="http://andeep/projects/neatpro/salary/export_salary" class="btn btn btn-info"><i class="fa fa-external-link "></i> Export To Excel</a>
      </div> -->
     <!--  <div class="hr hr-double hr-dotted hr18"></div> -->
      <div class="col-md-12 well">
        <legend> SEARCH BILL By One Of These Options</legend>
        <form>
          <fieldset>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label class="col-sm-4">Bill No.</label>
                <input type="number" class="col-sm-8" name="bill_no" id="bill_no" placeholder="Enter Bill No." onkeyup="tablefilter()" >
              </div>
            </div>
            <!-- <div class="col-md-5">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-4">Customer Code No.</label>
                  <input type="number" class="col-sm-8" name="code_no" id="code_no" placeholder="Enter Customer Code" onkeyup="tablefilter()" disabled>
                </div>
              </div>
            </div> -->
            <div class="space-24"></div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="col-sm-3">Customer Name:</label>
                <input type="text" class="col-sm-9" name="customer_name" id="customer_name" placeholder="Enter Customer Name" onkeyup="tablefilter()" >
              </div>
            </div>
            <!-- <div class="col-md-3">
              <div class="form-group">
                  <select name="staff_name" id="staff_name" class="chosen-select form-control" onchange="tablefilter()" disabled>
                    <option value="">All Collector</option>
                    @foreach($staffs as $staff)
                      <option value="{{$staff->id}}">{{$staff->name}}</option>
                    @endforeach
                  </select>
              </div>
            </div> -->
            <!-- <div class="col-md-4">
              <div class="form-group">
                <select name="marga" id="marga" class="chosen-select form-control" onchange="tablefilter()" disabled>
                  <option value="">All Marga</option>
                  @foreach($margas as $marga)
                      <option value="{{$marga->id}}">{{$marga->name}}</option>
                    @endforeach
                </select>
            </div>
          </div> -->
          <div class="space-24"></div>
          </div>
          </fieldset>
        </form>
      </div>
      <div class="hr hr-double hr-dotted hr18"></div>
      <div class="col-md-12 well">
        <div id="list_search_result" class="table-responsive">
          <table class="table table-striped table-bordered table-hover table-resposnive" id="festival_bill_table">
            <thead>
              <tr>
                
                <th>Date</th>
                <th>Name</th>
                <th>Collector</th>
                <th>Marga</th>
                <th>Bill No.</th>
                <th>Paid Amount</th>
                <th>Remarsk</th>
                <!-- <th>Action</th> -->
              </tr>
            </thead>
            
          </table>
        </div>
      </div>
    </div>
  </div><!-- /.row -->
</div>
@endsection
@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('js/chosen.proto.min.js')}}"></script>
<script>
  var url="{{asset('festivalbill/getdata')}}";
  var currentURL= window.location.href;
  var table = $('#festival_bill_table').DataTable({
    ajax:url, 
    columns:[
      {data:'date'},
      {data:'customer_name',searchable:true},
      {data:'staffs.name'}, 
      {data:'margas.name'},
      {data:'bill_no'},
      {data:'paid_amount',orderable:false,searchable:false},
      {data:'remarks',orderable:false,searchable:false}
    ],
  });
  function tablefilter(){
    var bill_no=$('#bill_no').val();
    var c_name=$('#customer_name').val();
    table.ajax.url(url+'?b_no='+bill_no+'&c_name='+c_name).load();
  }


  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize
  }

</script>
@endpush