<fieldset>
	{{csrf_field()}}
	<div id="list_search_result">
		<div class="col-md-3">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td>Customer Id</td>
						<td>
							<b>{{$customer->id}}</b>
							<input type="hidden" autocomplete="off" name="customer_id" class="form-control" value="{{$customer->id}}">
						</td>
					</tr>
					<tr>
						<td>Code No.</td>
						<td>
							<b>{{$customer->code_no}}</b>
							<input type="hidden" autocomplete="off" name="code_no" id="code_no" class="form-control" value="{{$customer->code_no}}">
						</td>
					</tr>
					<tr>
						<td>Name</td>
						<td>
							<b>{{$customer->name}}</b>
							<input type="hidden" autocomplete="off" id="customer_name" name="customer_name" class="form-control" value="{{$customer->name}}">
						</td>
					</tr>
					<tr>
						<td>Join Date.</td>
						<td>
							<b>{{$customer->date}}</b>
						</td>
					</tr>
					<tr>
						<td>House No.</td>
						<td>
							<b>{{$customer->house_no}}</b>
						</td>
					</tr>
					<tr>
						<td>Ward No.</td>
						<td>
							<b>{{$customer->ward_no}}</b>
						</td>
					</tr>
					<tr>
						<td>Marga</td>
						<td>
							<b>{{$customer->margas['name']}}</b>
							<input type="hidden" autocomplete="off" name="marga_name" id="marga_name" class="form-control" readonly="" value="{{$customer->margas['id']}}">
						</td>
					</tr>
					<tr>
						<td>Collector</td>
						<td>
							<b>{{$customer->margas->staffs['name']}}</b>
							<input type="hidden" autocomplete="off" name="staff_name" id="staff_name" class="form-control" value="{{$customer->margas->staffs['id']}}">
						</td>
					</tr>
					<tr>
						<td>Monthly Charge</td>
						<td>
							<b>{{$customer->monthly_charge}}</b>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-9"> 
			<div class="form-group">
				<label class="control-label col-md-2" for="bill_no">Bill No<span style="color: red;">*</span></label>
				<div class="col-md-4">
					<input type="number" autocomplete="off" name="bill_no" id="bill_no" class="form-control" required="required" value="">
				</div>
				<label class="control-label col-md-2">Date<span style="color: red;">*</span></label>
				<div class="col-md-4">
					<input type="text" name="date" class="form-control date-picker" id="date" required>
					<span class="text-danger">{{$errors->first('date')}}</span>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="rate">Paid Amount<span style="color: red;">*</span></label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="paid_amount" id="paid_amount" class="form-control" value="" required="">
				</div>
			</div>         
			<div class="form-group">
				<label class="control-label col-md-2" for="remarks">Remarks</label>
				<div class="col-md-10">
					<textarea class="form-group col-md-12" name="remarks"></textarea>
				</div>
			</div>
			<hr>
		</div>
		
		<div class="col-md-8 col-md-offset-4">
			<div class="col-md-4">
				<input type="submit" class="btn btn-success col-md-12" name="save_bill" value="Post Bill"/>
			</div>
			<div class="col-md-4">
				<a href="" class="btn btn-danger col-md-12">Cancel</a>
			</div>
		</div>
	</div>
</fieldset>