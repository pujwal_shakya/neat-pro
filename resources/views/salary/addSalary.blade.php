@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>
		<li>
			<a href="">List Salary</a>
		</li>
		<li>
			<a href="">Add Salary</a>
		</li>		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Succesfull !! &nbsp;</strong>{{Session::get('success')}}
	</div>
	@endif
	@if(Session::get('error'))
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Sorry !! &nbsp;</strong>{{Session::get('error')}}
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12 well">
			<div class="col-md-6">
				<legend> Select Staff to Post Salary</legend>
				<select class="chosen-select form-control" id="marga" name="marga" 
				onChange="myfunction($(this).val())">
					<option value="" disabled="disable" selected="selected">Please Select Staff</option>
					@foreach($staffs as $staff)
						<option value="{{$staff->id}}">{{$staff->name}}</option>
					@endforeach
				</select>
				<div class="hr hr-double hr-dotted hr18"></div>
			</div>
			<form role="form" class="form-horizontal" action="{{asset('salary/submit')}}" 
			method="post"  id="salary_table">
			{{csrf_field()}}
				<div id="list_search_result" class="col-md-12">
					<div class="col-xs-5">
						<legend class="center">STAFF DETAILS</legend>
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td>Staff Id</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Name</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Marital Status</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Designation</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Staff Type</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Basic Salary</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Performance Allowance</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Communication Allowance</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Medical Allowance</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Expensive Allowance</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Waste Management</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Provident Fund</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Less Provident Fund</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Total Taxable Amount</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Less TDS by 1%</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Advance</td>
										<td>
											<b></b>
										</td>
									</tr>
									<tr>
										<td>Basic Monthly Salary Reciept</td>
										<td>
											<b></b>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-xs-7">
						<legend class="center">POST SALARY</legend>
						<div class="table-responsive">
							<fieldset>
								<div class="form-group">
									<!-- <div class="col-sm-3">
										<label class="control-label col-md-6" for="voucher_id">V.no.</label>
										<div class="col-md-6">
											<input type="text" autocomplete="off" id="voucher_id" name="voucher_id" class="form-control" value="">
										</div>
									</div> -->
									<label class="control-label col-md-2">Date</label>
									<div class="col-md-4">
										<input type="text" name="date" class="form-control date-picker" id="date" required>
										<span class="text-danger">{{$errors->first('date')}}</span>
									</div>
									<label class="control-label col-md-2">Paid Up Salary</label>
									<div class="col-md-4">
										<input type="text" autocomplete="off" id="basic_total_amount" name="basic_total_amount" class="form-control" value="" readonly="">
									</div>		
								</div>
								<div class="form-group">
									<label class="control-label col-md-2">Annual/Festival Bonus</label>
									<div class="col-md-4">
										<input type="number" autocomplete="off" id="bonus" name="bonus" class="form-control" >
									</div>
									<label class="control-label col-md-2">Comission</label>
									<div class="col-md-4">
										<input type="text" autocomplete="off" id="comission" name="comission" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-2">Less Leave</label>
									<div class="col-md-4">
										<input type="number" autocomplete="off" id="less_leave" name="less_leave" class="form-control">
									</div>
									<label class="control-label col-md-2">Less Fooding</label>
									<div class="col-md-4">
										<input type="number" autocomplete="off" id="less_fooding" name="less_fooding" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-2">Over Time</label>
									<div class="col-md-4">
										<input type="number" autocomplete="off" id="over_time" name="over_time" class="form-control">
									</div>
									<label class="control-label col-md-2">Additional Deduction</label>
									<div class="col-md-4">
										<input type="number" autocomplete="off" id="addit_deduct" name="addit_deduct" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-2">Grand Total</label>
									<div class="col-md-4">
										<input type="text" autocomplete="off" id="grand_total" name="grand_total" class="form-control" value="" readonly="">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-2" for="remarks">Remarks
									</label>
									<div class="col-md-10">
										<textarea class="form-control" id="remarks" name="remarks"></textarea>
									</div>
								</div>
								<div class="hr hr-18 dotted hr-double"></div>
								<!-- <div class="col-md-8 col-md-offset-4">
									<div class="col-md-4">
										<input type="submit" name="save_staff" value="Post Salary" class="btn btn-success col-md-12" >
									</div>
									<div class="col-md-4">
										<a href="{{asset('staff')}}" class="btn btn-danger col-md-12">Cancel</a>
									</div>
								</div> -->
							</fieldset> 
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@push('script')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
	$('.date-picker').datepicker({
		format:'yyyy-mm-dd',
		autoclose: true,
		todayHighlight:true
	})
	if(!ace.vars['touch']) {
		$('.chosen-select').chosen({allow_single_deselect:true});
	}

	$('#salary_table').validate({
		errorElement: 'div',
		errorClass: 'help-block',
		highlight: function (e) {
			$(e).closest('div').removeClass('has-info').addClass('has-error');
		},
	});
	function myfunction(id){
		$.ajax({
			url:"{{asset('salary/staff_detail')}}/"+id,
			type:'get',
			success:function(response){
				$('#salary_table').html(response);
				$('.date-picker').datepicker({
					format:'yyyy-mm-dd',
					autoclose: true,
					todayHighlight:true
				})
			}
		});
	}
	function calculation(){
		var grand_total= $('#basic_total_amount').val();
		var bonus = $('#bonus').val();
		if(bonus.length==0){
			bonus=0;
		}
		var comission =$('#comission').val();
		if(comission.length==0){
			comission=0;
		}
		var less_leave= $('#less_leave').val();
		if(less_leave.length==0){
			less_leave=0;
		}
		var less_fooding =$('#less_fooding').val();
		if(less_fooding.length==0){
			less_fooding=0;
		}
		var over_time= $('#over_time').val();
		if(over_time.length==0){
			over_time=0;
		}
		var addit_deduct=$('#addit_deduct').val();
		if(addit_deduct.length==0){
			addit_deduct=0;
		}
		var total = parseInt(bonus)+parseInt(comission)-parseInt(less_leave)-parseInt(less_fooding)+parseInt(over_time)-parseInt(addit_deduct)+parseInt(grand_total);
		$('#grand_total').val(total);
	}
</script>

@endpush