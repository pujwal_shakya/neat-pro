@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>
		<li>
			<a href="">List Advance Salary</a>
		</li>
		<li>
			<a href="">Add Advance Salary</a>
		</li>		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Succesfull !! &nbsp;</strong>{{Session::get('success')}}
	</div>
	@endif
	@if(Session::get('error'))
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Sorry !! &nbsp;</strong>{{Session::get('error')}}
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12 well">
			<div class="hr hr-double hr-dotted hr18"></div>
			<div class="col-md-12 well">
				<div class="col-md-12" id="list_search_result">
					<form role="form" class="form-horizontal" action="{{asset('salary/advsalary/update')}}/{{$advance->id}}"
					 method="post" id="advSalary_table">
						{{csrf_field()}}
						<fieldset>
							<div class="form-group">
								<label class="control-label col-md-2" for="staff_id">Staff ID</label>
								<div class="col-md-4">
									<input type="text" autocomplete="off" id="staff_id" name="staff_id" class="form-control" required="" placeholder="Staff Id" value="{{$advance->id}}" readonly="">
								</div>
								<label class="control-label col-md-2" for="staff_name">Name</label>
								<div class="col-md-4">
									<input type="text" autocomplete="off" name="staff_name" id="staff_name" class="form-control" placeholder="Name" value="{{$advance->staffs['name']}}" readonly="">
								</div>
							</div>
							<div class="hr hr-18 dotted hr-double"></div>
							<div class="form-group">
								<label class="control-label col-md-2" for="date">Date</label>
								<div class="col-md-4">
									<input type="text" name="date" value="{{$advance->date}}" class="form-control date-picker" id="date">
								</div>
								<label class="control-label col-md-2" for="degsination">Designation</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="degsination" id="degsination" placeholder="Designation" value="{{$advance->staffs->designations['designation']}}" readonly="">
								</div>
							</div>
							<div class="hr hr-18 dotted hr-double"></div>
							<div class="form-group">
								<label class="control-label col-md-2" for="basic_salary">Basic Salary</label>
								<div class="col-md-4">
									<input type="number" class="form-control" name="basic_salary" id="basic_salary" placeholder="basic-salary" value="{{$advance->staffs['salary']}}" readonly="">
								</div>
								<label class="control-label col-md-2" for="amount">Advance Amount</label>
								<div class="col-md-4">
									<input type="number" class="form-control" name="adv_amount" id="adv_amount" placeholder="Amount" value="{{$advance->amount}}" required="">
								</div>
							</div>
							<div class="hr hr-18 dotted hr-double"></div>
							<div class="form-group">
								<label class="control-label col-md-2" for="remarks">Remarks</label>
								<div class="col-md-10">
									<textarea class="form-control" value="{{$advance->remarks}}" name="remarks" id="remarks"></textarea>
								</div>
							</div>
							<hr>
							<div class="col-md-8 col-md-offset-4">
								<div class="col-md-4">
									<input type="submit" class="btn btn-success col-md-12" name="save_advance" value="Post Advance">
								</div>
								<div class="col-md-4">
									<a href="http://andeep/projects/neatpro/salary/advance" class="btn btn-danger col-md-12">Cancel</a>
								</div>
							</div>
						</fieldset>
					</form> 
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
	$('.date-picker').datepicker({
		format:'yyyy-mm-dd',
		autoclose: true,
		todayHighlight:true
	})
	if(!ace.vars['touch']) {
		$('.chosen-select').chosen({allow_single_deselect:true});
	}

	function myfunction(id){
		$.ajax({
			url:"{{asset('salary/advsalary/staff_detail')}}/"+id,
			type:'get',
			success:function(response){
				$('#advSalary_table').html(response);
				$('.date-picker').datepicker({
					format:'yyyy-mm-dd',
					autoclose: true,
					todayHighlight:true
				})
			}
		});
	}

</script>

@endpush
