@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="">List Salary</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="well">
				<a href="{{asset('salary/add_salary')}}" class="btn btn-success">
					<i class="fa fa-plus"></i>
					Post Salary
				</a>
				
				<div class="hr hr-double hr-dotted hr18"></div>
				<legend> SEARCH</legend>
				<div class="row">
	                <div class="col-md-12">
	                    <div class="form-group">
	                        <input type="text"  name="staff_id" id="s_b_i" placeholder="Search By Staff ID" 
	                        onkeyup="filter()">
	                        <input type="text"  name="search_by_name" id="s_b_n" placeholder="Search By Name"
	                        onkeyup="filter()">
	                    </div>
	                </div>
	                
	                <div class="space-24"></div>
            	</div>
				<div class=" well">
					<div id="list_search_result" class="table-responsive"><!--  -->
						<table class="table table-striped table-bordered table-hover" id="salary_table">
							<thead>
								<tr>
									<th>Date</th>
									<th>Name</th>
									<th>Designation</th>
									<th>Basic Salary</th>
									<th>Performance Allowance</th>
									<th>Communication Allowance</th>
									<th>Expenses Allowance</th>
									<th>Medical Allowance</th>
									<th>Waste Management</th>
									<th>Bonus</th>
									<th>Comission</th>
									<th>Over Time</th>
									<th>Provident Fund</th>
									<th>Less Advance</th>
									<th>Less Leave</th>
									<th>Less Fooding</th>
									<th>Additional Deduction</th>
									<th>Less TDS</th>
									<th>Total</th>
								</tr>
							</thead>
							<!-- <tbody id="search_result">
								@foreach($salaries as $key=>$salary)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$salary->date}}</td>
									<td>{{$salary->staffs['name']}}</td>
									<td>{{$salary->staffs->designations['designation']}}</td>
									<td>{{$salary->salary}}</td>
									<td>{{$salary->performance_allowance}}</td>
									<td>{{$salary->communication_allowance}}</td>
									<td>{{$salary->expensive_allowance}}</td>
									<td>{{$salary->medical_allowance}}</td>
									<td>{{$salary->waste_mgmt}}</td>
									<td>{{$salary->bonus_amount}}</td>
									<td>{{$salary->comission_amount}}</td>
									<td>{{$salary->overtime_amount}}</td>
									<td>{{$salary->pro_fund}}</td>
									<td>{{$salary->advance}}</td>
									<td>{{$salary->leave_amount}}</td>
									<td>{{$salary->food_amount}}</td>
									<td>{{$salary->additional_deduct}}</td>
									<td>{{$salary->tax}}</td>
									<td>{{$salary->grand_total}}</td>
								</tr>
								@endforeach
							</tbody> -->
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script>
	function Remove(id){
		if(confirm('Are You Sure Want To Remove?')==false){
			return false;
		}else{
			$.ajax({
				url:"{{asset('staff/remove')}}/"+id,
				type:'get',
				success:function(response){
					$('#'+id).remove();
				}
			});
		}
	}
	
		var url="{{asset('salary/data')}}";
		var currentURL= window.location.href;
		var params=currentURL.split("?");
		var query=params[1];
		var table=$('#salary_table').DataTable({
			ajax:url+'?'+params,
			columns:[
				{data:'date'},
				{data:'staff_name'},
				{data:'designation'},
				{data:'salary'},
				{data:'performance_allowance'},
				{data:'communication_allowance'},
				{data:'expensive_allowance'},
				{data:'medical_allowance'},
				{data:'waste_mgmt'},
				{data:'bonus_amount'},
				{data:'comission_amount'},
				{data:'overtime_amount'},
				{data:'pro_fund'},
				{data:'advance'},
				{data:'leave_amount'},
				{data:'food_amount'},
				{data:'additional_deduct'},
				{data:'tax'},
				{data:'grand_total'},
			],
		});

		function filter(){
			var name=$('#s_b_n').val();
			var id=$('#s_b_i').val();
			table.ajax.url( url+'?name='+name+'&id='+id ).load();
		};
	
	
	// $('#s_b_n').keyup(function(){
	// 	var name=$('#s_b_n').val();
	// 	$.ajax({
	// 		url:"{{asset('searchname')}}?name="+name,
	// 		type:"get",
	// 		// data:{name:name},
	// 		cache:false,
	// 		success:function(response){
				
	// 		},
	// 	});
	// });
</script>

@endpush