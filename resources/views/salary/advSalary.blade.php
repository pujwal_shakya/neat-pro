@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>
		<li>
			<a href="">List Advance Salary</a>
		</li>		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12 well">
			<a href="{{asset('salary/advsalary/post_advance')}}" class="btn btn-success">
				<i class="fa fa-plus"></i>
				Post Advance Salary
			</a>
			<!-- <div class="hr hr-double hr-dotted hr18"></div>
				<legend> SEARCH</legend>
				<div class="row">
	                <div class="col-md-12">
	                    <div class="form-group">
	                        <input type="text"  name="staff_id" id="s_b_i" placeholder="Search By Staff ID" 
	                        onkeyup="filter()">
	                        <input type="text"  name="search_by_name" id="s_b_n" placeholder="Search By Name"
	                        onkeyup="filter()">
	                    </div>
	                </div>
	                
	                <div class="space-24"></div>
            	</div> -->
			<div id="list_search_result"><!--  -->
				<div id="list_result">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Date</th>
								<th>Name</th>
								<th>Basic Salary</th>
								<th>Advance Amount</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($advances as $advance)
							<tr id="{{$advance->id}}">
								<td>{{$advance->date}}</td>
								<td>{{$advance->staffs['name']}}</td>
								<td>{{$advance->basic_salary}}</td>
								<td>{{$advance->amount}}</td>
								<td>
									@if($advance->status==1)
										Advanced Not Cleared
									@else
										Advance Cleared
									@endif
								</td>
								<td>
									@if($advance->status==1)
										<a href="{{asset('salary/advsalary/edit')}}/{{$advance->id}}" class="edit-faculty btn btn-xs btn-warning">
											<span class="glyphicon glyphicon-eye-open"></span>
											Edit
										</a>
										<a role="button" class="btn btn-xs btn-danger remove_marga" onclick="Delete({{$advance->id}})" id="{{$advance->id}}">
	                                        <span class="ace-icon fa fa-trash-o bigger-120"></span>
	                                        Delete
	                                    </a>
	                                    @else
	                                       <span class="ace-icon fa fa-close bigger-120"></span>
	                                       <span class="ace-icon fa fa-close bigger-120"></span>
	                                       <span class="ace-icon fa fa-close bigger-120"></span>
                                    @endif
								</td>
							</tr>   
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('script')
<script>
	function Delete(id){
	    if(confirm('Are You Sure Want To Remove?')==false){
	        return false;
	    }else{

	        $.ajax({
	            url:"{{asset('salary/advsalary/delete')}}/"+id,
	            type:'get',
	            success:function(response){
	                $('#'+id).remove();
	            }
	        });
	    }
	}
	function filter(){
		var name=$('#s_b_n').val();
		var id=$('#s_b_i').val();
		table.ajax.url( url+'?name='+name+'&id='+id ).load();
	};
</script>
@endpush