<?php
	$pf=((float)$staff->salary/100) * 10;
	$less_pf=(float)$staff->salary/100*20;
	$total_salary=(float)$staff->salary+$pf-$less_pf+(float)$staff->performance_allowance+(float)$staff->communication_allowance+(float)$staff->medical_allowance+(float)$staff->expensive_allowance+(float)$staff->waste_mgmt;
	$advance=(float)$staff->advSalaries()->sum('amount')-(float)$staff->salaries()->sum('advance');
	$exclude_tax=$total_salary;

	if($staff->m_status=='unmarried'){
		if($staff->salary <= 50000){
			$tax_per = 1;
			$tax_amt=1/100*(float)$exclude_tax;	
		}elseif($staff->salary <= 100000){
			$tax_per = 2;
			$tax_amt=2/100*(float)$exclude_tax;	
		}else{
			$tax_per = 3;
			$tax_amt=3/100*(float)$exclude_tax;	
		}
		
	}
	else{
		if($staff->salary <= 50000){
			$tax_per = 4;
			$tax_amt=4/100*(float)$exclude_tax;	
		}elseif($staff->salary <= 100000){
			$tax_per = 5;
			$tax_amt=5/100*(float)$exclude_tax;	
		}else{
			$tax_per = 6;
			$tax_amt=6/100*(float)$exclude_tax;	
		}
	}
	$paid_up=$exclude_tax-$tax_amt-$advance;

?>
{{csrf_field()}}
<div id="list_search_result" class="col-md-12">
	<div class="col-xs-5">
		<legend class="center">STAFF DETAILS</legend>
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td>Staff Id</td>
						<td>
							<b>{{$staff->id}}</b>
							<input type="hidden" name="staff_id" id="staff_id" value="{{$staff->id}}">
						</td>
					</tr>
					<tr>
						<td>Name</td>
						<td>
							<b>{{$staff->name}}</b>
							<input type="hidden" name="staff_name" id="staff_name" value="{{$staff->name}}">
						</td>
					</tr>
					<tr>
						<td>Marital Status</td>
						<td>
							<b>{{$staff->m_status}}</b>
							<input type="hidden" name="m_status" id="m_status" value="{{$staff->m_status}}">
						</td>
					</tr>
					<tr>
						<td>Designation</td>
						<td>
							<b>{{$staff->designations['designation']}}</b>
							<input type="hidden" name="staff_desig" id="staff_desig" value="{{$staff->designations['designation']}}">
						</td>
					</tr>
					<tr>
						<td>Staff Type</td>
						<td>
							@if($staff->s_type=='full')
								<b>Full Time</b>
							@elseif($staff->s_type=='part')
								<b>Part Time</b>
							@elseif($staff->s_type=='contracted')
								<b>Contracted</b>
							@endif
							<input type="hidden" name="s_type" id="s_type" value="{{$staff->s_type}}">
						</td>
					</tr>
					<tr>
						<td>Basic Salary</td>
						<td>
							<b>{{$staff->salary}}</b>
							<input type="hidden" name="salary" id="salary" value="{{$staff->salary}}">
						</td>
					</tr>
					<tr>
						<td>Performance Allowance</td>
						<td>
							<b>{{$staff->performance_allowance}}</b>
							<input type="hidden" name="per_allo" id="per_allo" value="{{$staff->performance_allowance}}">
						</td>
					</tr>
					<tr>
						<td>Communication Allowance</td>
						<td>
							<b>{{$staff->communication_allowance}}</b>
							<input type="hidden" name="comm_allo" id="comm_allo" value="{{$staff->communication_allowance}}" readonly="">
						</td>
					</tr>
					<tr>
						<td>Medical Allowance</td>
						<td>
							<b>{{$staff->medical_allowance}}</b>
							<input type="hidden" name="med_allo" id="med_allo" value="{{$staff->medical_allowance}}">
						</td>
					</tr>
					<tr>
						<td>Expensive Allowance</td>
						<td>
							<b>{{$staff->expensive_allowance}}</b>
							<input type="hidden" name="exp_allo" id="exp_allo" value="{{$staff->expensive_allowance}}" readonly="">
						</td>
					</tr>
					<tr>
						<td>Waste Management</td>
						<td>
							<b>{{$staff->waste_mgmt}}</b>
							<input type="hidden" name="wst_allo" id="wst_allo" value="{{$staff->waste_mgmt}}">
						</td>
					</tr>
					
					<tr>
						<td>Add Provident Fund (10% of Basic Salary )</td>								
						<td>
							<b>{{$pf}}</b>
							<input type="hidden" name="pf_amt" id="pf_amt" value="{{$pf}}">
						</td>
					</tr>
					<tr>
						<td>Less Provident Fund (20% of Basic Salary)</td>					
						<td>
							<b class="text-danger">{{$less_pf}}</b>
							<input type="hidden" name="les_pf" id="les_pf" value="{{$less_pf}}">
						</td>
					</tr>
					<tr>
						<td>Total Salary</td>
						<td>					
							<b>{{$total_salary}}</b>
							<input type="hidden" name="taxable_amt" id="taxable_amt" value="{{$total_salary}}">
						</td>
					</tr>
					<tr>
						<td>Salary Amount Without Tax</td>
						<td>					
							<b>{{$exclude_tax}}</b>
							<input type="hidden" name="taxable_amt" id="taxable_amt" value="{{$exclude_tax}}">
						</td>
					</tr>
					<tr>
						<td>Less TDS by {{$tax_per}}%</td>
						<td>					
							<b class="text-danger">{{$tax_amt}}</b>
							<input type="hidden" name="tax_amt" id="tax_amt" value="{{$tax_amt}}">
						</td>
					</tr>
					<tr>
						<td>Advance</td>
						<td>
							<b class="text-danger">{{$advance}}</b>
							<input type="hidden" name="advance" id="advance" value="{{$advance}} ">
						</td>
					</tr>
					<tr>
						<td>Paid Up Salary</td>
						<td>	
							<b class="text-success">{{$paid_up}}</b>
							<input type="hidden" name="paid_up" id="paid_up" value="{{$paid_up}}">
						</td>
					</tr>
				</tbody>	
			</table>
		</div>
	</div>
	<div class="col-xs-7">
		<legend class="center">POST SALARY</legend>
		<div class="table-responsive">
			<fieldset>
				<div class="form-group">
					<!-- <div class="col-sm-3">
						<label class="control-label col-md-6" for="voucher_id">V.no.</label>
						<div class="col-md-6">
							<input type="text" autocomplete="off" id="voucher_id" name="voucher_id" class="form-control" value="">
						</div>
					</div> -->
					<label class="control-label col-md-2">Date<span style="color: red;">*</span></label>
					<div class="col-md-4">
						<input type="text" name="date" class="form-control date-picker" id="date" onmouseleave ="calculation()" required>
						<span class="text-danger">{{$errors->first('date')}}</span>
					</div>
					<label class="control-label col-md-2">Paid Up Salary</label>
					<div class="col-md-4">
						<input type="text" autocomplete="off" id="basic_total_amount" name="basic_total_amount" class="form-control" value="{{$paid_up}}" readonly="">
					</div>		
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Annual/Festival Bonus</label>
					<div class="col-md-4">
						<input type="number" autocomplete="off" id="bonus" name="bonus" class="form-control" onkeyup="calculation()">
					</div>
					<label class="control-label col-md-2">Comission</label>
					<div class="col-md-4">
						<input type="number" autocomplete="off" id="comission" name="comission" class="form-control" onkeyup="calculation()">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Less Leave</label>
					<div class="col-md-4">
						<input type="number" autocomplete="off" id="less_leave" name="less_leave" class="form-control" onkeyup="calculation()">
					</div>
					<label class="control-label col-md-2">Less Fooding</label>
					<div class="col-md-4">
						<input type="number" autocomplete="off" id="less_fooding" name="less_fooding" class="form-control" onkeyup="calculation()">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Over Time</label>
					<div class="col-md-4">
						<input type="number" autocomplete="off" id="over_time" name="over_time" class="form-control" onkeyup="calculation()">
					</div>
					<label class="control-label col-md-2">Additional Deduction</label>
					<div class="col-md-4">
						<input type="number" autocomplete="off" id="addit_deduct" name="addit_deduct" class="form-control" onkeyup="calculation()">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2">Grand Total</label>
					<div class="col-md-4">
						<input type="text" autocomplete="off" id="grand_total" name="grand_total" class="form-control" value="" readonly="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-2" for="remarks">Remarks
					</label>
					<div class="col-md-10">
						<textarea class="form-control" id="remarks" name="remarks"></textarea>
					</div>
				</div>
				<div class="hr hr-18 dotted hr-double"></div>
				<div class="col-md-8 col-md-offset-4">
					<div class="col-md-4">
						<input type="submit" name="save_staff" value="Post Salary" class="btn btn-success col-md-12" >
					</div>
					<div class="col-md-4">
						<a href="{{asset('staff')}}" class="btn btn-danger col-md-12">Cancel</a>
					</div>
				</div>
			</fieldset> 
		</div>
	</div>
</div>
