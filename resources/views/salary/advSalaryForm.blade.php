{{csrf_field()}}
<fieldset>
	<div class="form-group">
		<label class="control-label col-md-2" for="staff_id">Staff ID</label>
		<div class="col-md-4">
			<input type="text" autocomplete="off" id="staff_id" name="staff_id" class="form-control" required="" placeholder="Staff Id" value="{{$staff->id}}" readonly="">
		</div>
		<label class="control-label col-md-2" for="staff_name">Name</label>
		<div class="col-md-4">
			<input type="text" autocomplete="off" name="staff_name" id="staff_name" class="form-control" placeholder="Name" value="{{$staff->name}}" readonly="">
		</div>
	</div>
	<div class="hr hr-18 dotted hr-double"></div>
	<div class="form-group">
		<label class="control-label col-md-2" for="date">Date<span style="color: red;">*</span></label>
		<div class="col-md-4">
			<input type="text" name="date" class="form-control date-picker" id="date" required>
		</div>
		<label class="control-label col-md-2" for="degsination">Designation</label>
		<div class="col-md-4">
			<input type="text" class="form-control" name="degsination" id="degsination" placeholder="Designation" value="{{$staff->designations['designation']}}" readonly="">
		</div>
	</div>
	<div class="hr hr-18 dotted hr-double"></div>
	<div class="form-group">
		<label class="control-label col-md-2" for="basic_salary">Basic Salary</label>
		<div class="col-md-4">
			<input type="number" class="form-control" name="basic_salary" id="basic_salary" placeholder="basic-salary" value="{{$staff->salary}}" readonly="">
		</div>
		<label class="control-label col-md-2" for="amount">Advance Amount<span style="color: red;">*</span></label>
		<div class="col-md-4">
			<input type="number" class="form-control" name="adv_amount" id="adv_amount" placeholder="Amount" required="">
		</div>
	</div>
	<div class="hr hr-18 dotted hr-double"></div>
	<div class="form-group">
		<label class="control-label col-md-2" for="remarks">Remarks</label>
		<div class="col-md-10">
			<textarea class="form-control" name="remarks" id="remarks"></textarea>
		</div>
	</div>
	<hr>
	<div class="col-md-8 col-md-offset-4">
		<div class="col-md-4">
			<input type="submit" class="btn btn-success col-md-12" name="save_advance" value="Post Advance">
		</div>
		<div class="col-md-4">
			<a href="http://andeep/projects/neatpro/salary/advance" class="btn btn-danger col-md-12">Cancel</a>
		</div>
	</div>
</fieldset>