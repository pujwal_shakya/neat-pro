@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Home</a>
        </li>

        <li>
            <a href="{{asset('collection')}}">Collection Details</a>
        </li>
    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
    <div class="row">
        <div class="col-xs-12  well" id="list_search_result">
            <a href="{{asset('collection/post_collection')}}/{{$staff->id}}" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
            <h3 style="margin-left: 400px;">Collection Details of {{$staff->name}} </h3>
            <table class="table table-striped table-bordered table-hover table-resposnive" id="collection_table">
                <thead>
                    <h4 class="label label-xlg label-white middle">UnCollected Amount : {{$uncollected['balance']}}</h4>
                    <tr>
                        <th>Date</th>
                        <th>Bills From</th>
                        <th>Bills To</th>
                        <th>Amount To Collect</th>
                        <th>Amount Collected</th>
                        <th>Remaining</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
            </table>
        </div>
    </div><!-- /.row -->
</div>
@endsection
@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script>
    $(document).ready(function(){
        var url="{{asset('collection/staffs/data')}}?name="+{{$staff->id}};
        var currentUrl=window.location.href;
        var params=currentUrl.split("?");
        var query=params[1];
        $('#collection_table').dataTable({
            ajax:url,
            columns:[
                {data:'date'},
                {data:'bill_from'},
                {data:'bill_to'},
                {data:'dr_amt'},
                {data:'cr_amt'},
                {data:'balance'},
            ],
        });
    });
</script>
@endpush
