@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>
		<li>
			<a href="{{asset('collection')}}">Add Collection</a>
		</li>
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Succesfully !! &nbsp;</strong>{{Session::get('success')}}
	</div>
	@endif

	@if(Session::get('error'))
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Error !! &nbsp;</strong>{{Session::get('error')}}
	</div>
	@endif
	<div class="page-header">
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-md-12">
			<div class="table-header"><em style="color:red;">*</em> [ Denotes Mandatory Fields ]</div>
			<div class="well">
				<form role="form" class="form-horizontal" action="{{asset('collection/saveCollection')}}" method="post" id="collection_form">
					<fieldset>
					{{csrf_field()}}
						
						<div class="form-group">
							<label class="control-label col-md-2">Date<span style="color: red;">*</span></label>
							<div class="col-md-4">
								<input type="text" name="date" class="form-control date-picker" id="date" required>
								<span class="text-danger">{{$errors->first('date')}}</span>
							</div>
							<label class="control-label col-md-2" for="collector">Collector<span style="color: red;">*</span></label>
							<div class="col-md-4">
								<input type="hidden" value="{{$staff->id}}" name="staff_id"> 
								<input type="text" value="{{$staff->name}}" class="form-control" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="bill_from">Bill From<span style="color: red;">*</span></label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" id="bill_from" name="bill_from" class="form-control" required>
							</div>

							<label class="control-label col-md-2" for="bill_to">Bill To<span style="color: red;">*</span></label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" id="bill_to" name="bill_to" class="form-control" required>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2" for="no_of_bill">No. of Bills</label>
							<div class="col-md-4">
								<input type="text" autocomplete="off" id="no_of_bill" name="no_of_bill" class="form-control" readonly="">
							</div>
							<label class="control-label col-md-2" for="debit_amount">Amount To Collect<span style="color: red;">*</span></label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" id="debit_amount" name="debit_amount" class="form-control" required value="" onkeyup="total_balance()">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2" for="credit_amount">Collected Amount</label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" id="credit_amount" name="credit_amount" class="form-control" value="" onkeyup="total_balance()">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="prev_balance">Unreceived Amount</label>
							<div class="col-md-4">
								<input type="text" autocomplete="off" id="prev_balance" name="prev_blnc" class="form-control" readonly="" value="{{$prev['balance']}}" placeholder="Previous Balance">
							</div>
							<label class="control-label col-md-2" for="balance">Remaining</label>
							<div class="col-md-4">
								<input type="text" name="new_blnc" id="new_balance" class="form-control" readonly="" placeholder="New Balance">
								</div>
						</div>
						<div class="hr hr-double hr-dotted hr18"></div>
						<div class="col-md-8 col-md-offset-4">
							<div class="col-md-4">
								<input type="submit" class="btn btn-success col-md-12" name="save_collection" value="Post Collection">
							</div>
							<div class="col-md-4">
								<a href="http://andeep/projects/neatpro/collection" class="btn btn-danger col-md-12">Cancel</a>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
	$('.date-picker').datepicker({
		format:'yyyy-mm-dd',
		autoclose: true,
		todayHighlight:true
	})
	$('#bill_to').keyup(function(){
		var bill_to=$('#bill_to').val();
		var bill_from=$('#bill_from').val();
		if(bill_from.length==0){
			$bill_from=0;
		}
		if(bill_to.length==0){
			$bill_to=0;
		}
		var total_bill=parseInt(bill_to)-parseInt(bill_from)+1;
		$('#no_of_bill').val(total_bill);
	});
	function total_balance(){
		var debit=$('#debit_amount').val();
		var credit=$('#credit_amount').val();
		var prev_blnc=$('#prev_balance').val();
		if(debit.length==0){
			debit=0;
		} 
		if(credit.length==0){
			credit=0;
		}
		if(prev_blnc.length==0){
			prev_blnc=0;
		}
		var balance = parseInt(debit)-parseInt(credit)+parseInt(prev_blnc);
		$("#new_balance").val(balance);
	}
	$('#collection_form').validate({
	errorElement: 'div',
	errorClass: 'help-block',
	highlight: function (e) {
		$(e).closest('div').removeClass('has-info').addClass('has-error');
	},
});

</script>
@endpush