@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="">Collection Details</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12 well">
			<div class="table">
				<ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
					@foreach($staffs as $staff)
					<li class="">
						<a href="{{asset('collection/details')}}/{{$staff->id}}">
							<i class="blue ace-icon fa fa-folder  bigger-120"></i>
							{{$staff->name}}                              
						</a>
					</li>
					@endforeach
				</ul>
			</div>
			<div class=" col-sm-12 widget-container-col">
				<div class="widget-box">
					<div class="widget-header">
						<h5 class="widget-title">TOTAL COLLECTION</h5>
					</div>
					<div class="widget-body">
						<div class="widget-main">
							<h4 class="label label-xlg label-white middle">Total Collectable Amount:{{$debit}}</h4>
							<h4 class="label label-xlg label-white middle">Total Collected Amount:{{$credit}}</h4>
							<h4 class="label label-xlg label-white middle">UnCollected Amount:{{$total}}</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection