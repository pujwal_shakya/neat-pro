@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/nepali.datepicker.v2.2.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Home</a>
        </li>
        <li>
            <a href="">Bill List</a>
        </li>
        <li>
            <a href="">Post Bill</a>
        </li>
    </ul>
   <!-- /.breadcrumb -->
</div>
<div class="page-content">
    @if(Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Succesfully !! &nbsp;</strong>{{Session::get('success')}}
    </div>
    @endif

    @if(Session::get('error'))
    <div class="alert alert-error">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error !! &nbsp;</strong>{{Session::get('error')}}
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12  well">
            <!-- <div class="row">
                <div style="margin-left:100px">
                    <form method="post" action="http://andeep/projects/neatpro/bill/set_bill">
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="number" name="set_billno" class="form-control" required="required" placeholder="SET BILL NO.">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-primary" type="submit" name="set_bill">
                                    <i class="ace-icon fa   fa-bookmark  bigger-110"></i>
                                        SET BILL NO.
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <select class="chosen-select form-control" name="set_month" id="set_month">
                                <option value="" selected="" disabled="disabled">SET MONTH</option>
                                <option value="1">Baishak</option>
                                <option value="2">Jesth</option>
                                <option value="3">Ashad</option>
                                <option value="4">Sawaran</option>
                                <option value="5">Bhadra</option>
                                <option value="6">Ashwin</option>
                                <option value="7">kartik</option>
                                <option value="8">Mungsir</option>
                                <option value="9">Poush</option>
                                <option value="10">Magh</option>
                                <option value="11">Falgun</option>
                                <option value="12">Chitra</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div> -->
            <div class="space"></div>
            <div class="col-md-12 well">
                <div>
                    <legend> Filter Customer</legend>
                    <form>
                        <fieldset>
                            <div class="row">
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-2">Code No.</label>
                                        <input type="text" class="col-sm-8" name="staff_id" id="search_by_customer_code" disabled="" placeholder="filter by customer code">
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="chosen-select form-control" id="customer" name="customer" onChange="myfunction($(this).val())">
                                            <option value="" disabled="disable" selected="selected">Please Select Customer</option>
                                            @foreach($customers as $customer)
                                                <option value="{{$customer->id}}">{{$customer->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="space-24"></div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="hr hr-double hr-dotted hr18"></div>

                <form role="form" class="form-horizontal" action="{{asset('bill/save_bill_details')}}" method="post" id="customer_bill">
                    <fieldset>
                        <div id="list_search_result">
                            <div class="col-md-3">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <td>Customer Id</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Code No.</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Join Date.</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>House No.</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ward No.</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Marga</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Collector</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Monthly Charge</td>
                                        <td>
                                            <b>-</b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="monthly_fee">Monthly Charge</label>
                                    <div class="col-md-4">
                                        <input type="text" autofocus="autofocus" autocomplete="" name="monthly_charge" id="monthly_fee" class="form-control" required="" readonly="" onkeyup="calculation()">
                                    </div>
                                    <label class="control-label col-md-2" for="rate">Paid Amount</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="paid_amount" tabindex="45" id="paid_amount" class="form-control" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="bill_no">Bill No</label>
                                    <div class="col-md-4">
                                        <input type="number" autocomplete="off" name="bill_no" id="bill_no" class="form-control" required="required" value="" required="">
                                    </div>
                                    <label class="control-label col-md-2">Date</label>
                                    <div class="col-md-4">
                                        <input type="text" name="date" class="form-control date-picker" id="date" required>
                                        <span class="text-danger">{{$errors->first('date')}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="monthly_fee">Paid From</label>
                                    <div class="col-md-4">
                                        <select class="chosen-select form-control" name="paid_from" id="month" required>
                                            <option value="" selected="" disabled="disabled">Select Month</option>
                                            <option value="Baishak">Baishak</option>
                                            <option value="Jesth">Jesth</option>
                                            <option value="Ashad">Ashad</option>
                                            <option value="Sawaran">Sawaran</option>
                                            <option value="Bhadra">Bhadra</option>
                                            <option value="Ashwin">Ashwin</option>
                                            <option value="Kartik">Kartik</option>
                                            <option value="Mungsir">Mungsir</option>
                                            <option value="Poush">Poush</option>
                                            <option value="Magh">Magh</option>
                                            <option value="Falgun">Falgun</option>
                                            <option value="Chitra">Chitra</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-2" for="rate">Paid Upto</label>
                                    <div class="col-md-4">
                                        <select class="chosen-select form-control" name="paid_upto" id="month">
                                            <option value="" selected="" disabled="disabled">Select Month</option>
                                            <option value="Baishak">Baishak</option>
                                            <option value="Jesth">Jesth</option>
                                            <option value="Ashad">Ashad</option>
                                            <option value="Sawaran">Sawaran</option>
                                            <option value="Bhadra">Bhadra</option>
                                            <option value="Ashwin">Ashwin</option>
                                            <option value="Kartik">Kartik</option>
                                            <option value="Mungsir">Mungsir</option>
                                            <option value="Poush">Poush</option>
                                            <option value="Magh">Magh</option>
                                            <option value="Falgun">Falgun</option>
                                            <option value="Chitra">Chitra</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="member_fee">Member Fee</label>
                                    <div class="col-md-4">
                                        <input type="number" autocomplete="off" name="member_fee" id="member_fee" class="form-control">
                                    </div>
                                    <label class="control-label col-md-2" for="card_fee">Card Fee</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="card_fee" id="card_fee" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="drain_clean">Drain Cleaning Fee</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="drain_clean" id="drain_clean" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="late_fee">Late Fee</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="late_fee" id="late_fee" class="form-control">
                                    </div>
                                    <label class="control-label col-md-2" for="others">Others</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="others" id="others" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="service_charge">Service Charge</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="service_charge" id="service_charge" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="less_discount">Less Discount</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="less_discount" id="less_discount" class="form-control">
                                    </div>
                                    <label class="control-label col-md-2" for="grand_total">Grand Total</label>
                                    <div class="col-md-4">
                                        <input type="text" autocomplete="off" name="grand_total" id="grand_total" class="form-control" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="remarks">Remarks</label>
                                    <div class="col-md-10">
                                        <textarea class="form-group col-md-12" name="remarks"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <!-- <div class="col-md-8 col-md-offset-4">
                                   <div class="col-md-4">
                                       <input type="submit" class="btn btn-success col-md-12" name="save_bill" value="Post Bill"/>
                                   </div>
                                   <div class="col-md-4">
                                       <a href="http://andeep/projects/neatpro/neatpro/bill" class="btn btn-danger col-md-12">Cancel</a>
                                   </div>
                                   </div> -->
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript" src="{{asset('js/nepali.datepicker.v2.2.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('js/chosen.proto.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
    $('.date-picker').datepicker({
        format:'yyyy-mm-dd',
        autoclose: true,
        todayHighlight:true
    })
    if(!ace.vars['touch']) {
            $('.chosen-select').chosen({allow_single_deselect:true}); 
            //resize the chosen on window resize
        }
    function myfunction(id){
            $.ajax({
                url:"{{asset('bill/customer_detail')}}/"+id,
                type:'get',
                success:function(response){
                    $('#customer_bill').html(response);
                    $('.date-picker').datepicker({
                        format:'yyyy-mm-dd',
                        autoclose: true,
                        todayHighlight:true
                    })
                    if(!ace.vars['touch']) {
                        $('.chosen-select').chosen({allow_single_deselect:true}); 
                        //resize the chosen on window resize
                    }
                    $('#customer_bill').validate({
                        errorElement: 'div',
                        errorClass: 'help-block',
                        highlight: function (e) {
                        $(e).closest('.col-md-4').removeClass('has-info').addClass('has-error');
                        },
                    });

                }
            });
        }
        function calculation(){
            var service_charge=$("#service_charge").val();
            if(service_charge.length==0){
                service_charge=0;
            }
            var paid_amount = $('#paid_amount').val();
            if(paid_amount.length==0){
                paid_amount=0;
            }
            var card_fee =$("#card_fee").val();
            if(card_fee.length==0){
                card_fee=0;
            }
            var member_fee =$("#member_fee").val();
            if(member_fee.length==0){
                member_fee=0;
            }
            var drain_clean = $("#drain_clean").val();
            if(drain_clean.length==0) {
                drain_clean = 0;
            }
            var others = $("#others").val();
            if(others.length==0) {
                others = 0;
            }
            var late_fee = $("#late_fee").val();
            if(late_fee.length==0) {
                late_fee = 0;
            }
            var less_discount = $("#less_discount").val();
            if(less_discount.length==0) {
                less_discount = 0;
            }
            
                var total = parseInt(paid_amount)+ parseInt(member_fee)+parseInt(card_fee)+parseInt(drain_clean)+parseInt(late_fee)+parseInt(others)+parseInt(service_charge)-parseInt(less_discount);
                $("#grand_total").val(total);
            }
</script>
@endpush