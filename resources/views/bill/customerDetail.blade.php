<fieldset>
	{{csrf_field()}}
	<div id="list_search_result">
		<div class="col-md-3">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td>Customer Id</td>
						<td>
							<b>{{$customer->id}}</b>
							<input type="hidden" autocomplete="off" name="customer_id" class="form-control" value="{{$customer->id}}">
						</td>
					</tr>
					<tr>
						<td>Code No.</td>
						<td>
							<b>{{$customer->code_no}}</b>
							<input type="hidden" autocomplete="off" name="code_no" id="code_no" class="form-control" value="{{$customer->code_no}}">
						</td>
					</tr>
					<tr>
						<td>Name</td>
						<td>
							<b>{{$customer->name}}</b>
							<input type="hidden" autocomplete="off" id="customer_name" name="customer_name" class="form-control" value="{{$customer->name}}">
						</td>
					</tr>
					<tr>
						<td>Join Date.</td>
						<td>
							<b>{{$customer->date}}</b>
						</td>
					</tr>
					<tr>
						<td>House No.</td>
						<td>
							<b>{{$customer->house_no}}</b>
						</td>
					</tr>
					<tr>
						<td>Ward No.</td>
						<td>
							<b>{{$customer->ward_no}}</b>
						</td>
					</tr>
					<tr>
						<td>Marga</td>
						<td>
							<b>{{$customer->margas['name']}}</b>
							<input type="hidden" autocomplete="off" name="marga_name" id="marga_name" class="form-control" readonly="" value="{{$customer->margas['id']}}">
						</td>
					</tr>
					<tr>
						<td>Collector</td>
						<td>
							<b>{{$customer->margas->staffs['name']}}</b>
							<input type="hidden" autocomplete="off" name="staff_name" id="staff_name" class="form-control" value="{{$customer->margas->staffs['id']}}">
						</td>
					</tr>
					<tr>
						<td>Monthly Charge</td>
						<td>
							<b>{{$customer->monthly_charge}}</b>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-9">
			<div class="form-group">
				<label class="control-label col-md-2" for="monthly_fee">Monthly Charge</label>
				<div class="col-md-4">
					<input type="text" autofocus="autofocus" autocomplete="" name="monthly_charge" id="monthly_fee" value="{{$customer->monthly_charge}}" class="form-control" required="" readonly="">
				</div>
				<label class="control-label col-md-2" for="rate">Paid Amount<span style="color: red;">*</span></label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="paid_amount" id="paid_amount" class="form-control"  onkeyup="calculation()" required="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="bill_no">Bill No<span style="color: red;">*</span></label>
				<div class="col-md-4">
					<input type="number" autocomplete="off" name="bill_no" id="bill_no" class="form-control" required="required" value="">
				</div>
				<label class="control-label col-md-2">Date<span style="color: red;">*</span></label>
				<div class="col-md-4">
					<input type="text" name="date" class="form-control date-picker" id="date" required>
					<span class="text-danger">{{$errors->first('date')}}</span>
				</div>
			</div>
			<div class="form-group">
                <label class="control-label col-md-2" for="monthly_fee">Paid From<span style="color: red;">*</span></label>
                <div class="col-md-4">
                    <select class="chosen-select form-control" name="paid_from" id="month" required="">
                        <option value="" selected="" disabled="disabled">Select Month</option>
                        <option value="1">Baishak</option>
                        <option value="2">Jesth</option>
                        <option value="3">Ashad</option>
                        <option value="4">Sawaran</option>
                        <option value="5">Bhadra</option>
                        <option value="6">Ashwin</option>
                        <option value="7">Kartik</option>
                        <option value="8">Mungsir</option>
                        <option value="9">Poush</option>
                        <option value="10">Magh</option>
                        <option value="11">Falgun</option>
                        <option value="12">Chitra</option>
                    </select>
                </div>
                <label class="control-label col-md-2" for="rate">Paid Upto</label>
                <div class="col-md-4">
                    <select class="chosen-select form-control" name="paid_upto" id="month">
                        <option value="" selected="" disabled="disabled">Select Month</option>
                        <option value="1">Baishak</option>
                        <option value="2">Jesth</option>
                        <option value="3">Ashad</option>
                        <option value="4">Sawaran</option>
                        <option value="5">Bhadra</option>
                        <option value="6">Ashwin</option>
                        <option value="7">Kartik</option>
                        <option value="8">Mungsir</option>
                        <option value="9">Poush</option>
                        <option value="10">Magh</option>
                        <option value="11">Falgun</option>
                        <option value="12">Chitra</option>
                    </select>
                </div>
            </div>
			<div class="form-group">
				<label class="control-label col-md-2" for="member_fee">Member Fee</label>
				<div class="col-md-4">
					<input type="number" autocomplete="off" name="member_fee" id="member_fee" class="form-control" onkeyup="calculation()">
				</div>
				<label class="control-label col-md-2" for="card_fee">Card Fee</label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="card_fee" id="card_fee" class="form-control" onkeyup="calculation()">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="drain_clean">Drain Cleaning Fee</label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="drain_clean" id="drain_clean" class="form-control"onkeyup="calculation()">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="late_fee">Late Fee</label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="late_fee" id="late_fee" class="form-control"	onkeyup="calculation()">
				</div>
				<label class="control-label col-md-2" for="others">Others</label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="others" id="others" class="form-control" onkeyup="calculation()">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="service_charge">Service Charge</label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="service_charge" id="service_charge" class="form-control" onkeyup="calculation()">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="less_discount">Less Discount</label>
				<div class="col-md-4">
					<input type="text" autocomplete="off" name="less_discount" id="less_discount" class="form-control" onkeyup="calculation()">
				</div>
				<label class="control-label col-md-2" for="grand_total">Grand Total</label>
				<div class="col-md-4 ">
					<input type="text" autocomplete="off" name="grand_total" id="grand_total" class="form-control" readonly="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="remarks">Remarks</label>
				<div class="col-md-10">
					<textarea class="form-group col-md-12" name="remarks"></textarea>
				</div>
			</div>
			<hr>
            <div class="col-md-8 col-md-offset-4">
               	<div class="col-md-4">
                   <input type="submit" class="btn btn-success col-md-12" name="save_bill" value="Post Bill"/>
               	</div>
               	<div class="col-md-4">
                   <a href="" class="btn btn-danger col-md-12">Cancel</a>
               	</div>
           	</div>
       	</div>
   	</div>
</fieldset>