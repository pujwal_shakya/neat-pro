@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('report')}}">Report</a>
		</li>
		<li>
			<a href="">Due Report</a>
		</li>

	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12  well">
			<div class="col-md-12 well" id="list_search">
				<div>
					<legend>Search Annual Report</legend>
					<form action="{{asset('report/deu_report/search')}}" method="post">
						<fieldset>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<input type="text" id="code_no" class="form-control" placeholder="ENTER CODE NO.">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select id="year" class="form-control" name="year">
											<option value="" disabled="disabled" selected="">Select Year</option>
											<option value="2024">2024</option>
											<option value="2023">2023</option>
											<option value="2022">2022</option>
											<option value="2021">2021</option>
											<option value="2020">2020</option>
											<option value="2019">2019</option>
											<option value="2018">2018</option>
											<option value="2017">2017</option>
											<option value="2016">2016</option>
											<option value="2015">2015</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<button type="button" class="btn btn-sm btn-info" id="btn_search">Search</button>
								</div>
								<div class="space-24"></div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="hr hr-double hr-dotted hr18"></div>
				<div id="list_search_result">
					<h1>Please Select Customer Code &amp; Year to Search Due</h1>
				</div>
			</div>
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>

@endsection
@push('script')
<script>
	$('#btn_search').click(function(){
		var code=$('#code_no').val();
		var year=$('#year').val();
		var data={'code_no':code,'year':year,'_token':'{{csrf_token()}}'};
		$.ajax({
			type: "POST",
			url:"{{asset('report/due_report/getdata')}}",
			data:data,
			success:function(response){
				$('#list_search_result').html(response);
			}
		});
	});
</script>
@endpush