@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('report')}}">Report</a>
		</li>
		<li>
			<a href="">Annual Report</a>
		</li>

	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12  well">
			<div class="hr hr-double hr-dotted hr18"></div>
			<div class="col-md-12 well" id="list_search">
				<div>
					<legend>Search Annual Report</legend>
					<form>
						<fieldset>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<select id="year" class="form-control" name="year" 
										onChange="filter()">
											<option value="" disabled="disabled" selected="">Select Year</option>
											<option value="2024">2024</option>
											<option value="2023">2023</option>
											<option value="2022">2022</option>
											<option value="2021">2021</option>
											<option value="2020">2020</option>
											<option value="2019">2019</option>
											<option value="2018">2018</option>
											<option value="2017">2017</option>
											<option value="2016">2016</option>
											<option value="2015">2015</option>
										</select>
									</div>
								</div>
								<div class="space-24"></div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="hr hr-double hr-dotted hr18"></div>
				<div id="list_search_result">
					<h1>Please Select Year to View Annual Report</h1>
				</div>
			</div>
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
@endsection
@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script src="{{asset('js/dataTables.fixedColumns.min.js')}}"></script>
<script>
	function filter(){
		var year=$('#year').val();
		$.ajax({
			type:"get",
			url:"{{asset('report/annual_report/getdata')}}?date="+year,
			success:function(response){
				$('#list_search_result').html(response);
				var table=$('#annual').dataTable({
					scrollY:"300px",
					orderable:false,
			        scrollX:true,
			        scrollCollapse:true,
			        paging:false,
			        fixedColumns:{
			            leftColumns:1
			        }
				});
			}
		});
	}
</script>
@endpush