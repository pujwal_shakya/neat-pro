@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('marga')}}">Report</a>
		</li>

	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12  well">
			<div class="tabbable">
				<ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
					<li>
						<a href="{{asset('report/due_report')}}">
							<i class="blue ace-icon fa fa-credit-card bigger-120"></i>
							Customer Due Report
						</a>
					</li>
					<li class="">
						<a href="{{asset('report/annual_report')}}">
							<i class="blue ace-icon fa fa-tasks bigger-120"></i>
							Annual Report
						</a>
					</li>
					<li>
						<a href="http://andeep/projects/neatpro/report/collection_report">
							<i class="blue ace-icon fa fa-credit-card bigger-120"></i>
							Collection Report
						</a>
					</li>
					<li>
						<a href="http://andeep/projects/neatpro/report/general_income_report">
							<i class="blue ace-icon fa fa-credit-card bigger-120"></i>
							Income &amp; Expenses Report
						</a>
					</li>
				</ul>
			</div>
			<div class="space space24"></div>
			<!-- tab contends -->
			<div class="col-md-6">
				<div class="text-danger">
					<h1>SELECT TO VIEW REPORT</h1>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
</div>

@endsection