@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Home</a>
        </li>

        <li>
            <a href="{{asset('report')}}">Report</a>
        </li>
        <li>
            <a href="">Annual Report</a>
        </li>

    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
    <div class="row">
        <div class="col-xs-12  well">
           <div class="tabbable">
            <ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
                <li class="active">
                    <a href="{{asset('report/collection_report')}}">
                        <i class="blue ace-icon fa fa-tasks bigger-120"></i>
                        Marga Collection Report
                    </a>
                </li>
                <li>
                    <a href="http://andeep/projects/neatpro/report/staff_collection_report">
                        <i class="blue ace-icon fa fa-credit-card bigger-120"></i>
                        Staff Collection Report
                    </a>
                </li>
                <li>
                    <a href="http://andeep/projects/neatpro/report/monthly_collection_report">
                        <i class="blue ace-icon fa fa-credit-card bigger-120"></i>
                        Monthly Collection Report
                    </a>
                </li>
            </ul>
        </div>
        <div class="space space24"></div>
        <!-- tab contends -->
        <div class="col-md-12">
            <div class="well">
                <form method="post" action="http://andeep/projects/neatpro/report/search_marga_collection_record">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                   <select class="chosen-select form-control" name="marga" id="marga" data-placeholder="Choose a State..." style="display: none;">
                                   </select><div class="chosen-container chosen-container-single" style="width: 290px;" title="" id="marga_chosen"><a class="chosen-single" tabindex="-1"><span>Select Marga</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
                               </div>
                           </div>
                           <div class="col-md-4">
                            <div class="form-group">
                               <select class="chosen-select form-control" name="month" id="month" data-placeholder="Choose a State..." style="display: none;">
                               </select><div class="chosen-container chosen-container-single" style="width: 290px;" title="" id="month_chosen"><a class="chosen-single" tabindex="-1"><span>Select Month</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
                           </div>
                       </div>
                       <div class="col-md-2">
                        <div class="form-group">
                           <select class="chosen-select form-control" name="year" id="year" data-placeholder="Choose a State..." style="display: none;">
                           </select><div class="chosen-container chosen-container-single" style="width: 133px;" title="" id="year_chosen"><a class="chosen-single" tabindex="-1"><span>Select Year</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
                       </div>
                   </div>
                   <div class="col-md-2">
                    <button type="submit" class="btn btn-info btn-xs" name="search_btn">Search</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="space space16"></div>
<div>
    <a href="http://andeep/projects/neatpro/report/print_marga_monthly_report" class="btn btn-primary btn-sm"><i class="ace fa fa-print"></i> Print</a>
    <div class="space"></div>
    <?php

    ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Marga</th>
                <th>Total Customer</th>
                <th>Monthly Collection</th>
                <th>Annual Collection</th>
            </tr>
        </thead>
        <tbody>
            @foreach($margas as $marga)
                <tr>
                    <td>{{$marga->name}}</td>
                    <td>{{count($marga->customers)}}</td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- /.row -->
</div>
@endsection