<div class="space space-16"></div>
<table class="table  table-bordered table-resposnive stripe row-border order-column"   id="annual">
	<thead>
		<tr>
			<th rowspan="2">Name</th>
			<th rowspan="2">Code No.</th>
			<th colspan="3" class="mm-1 center">Baishak</th>
			<th colspan="3" class="mm-2">Jesth</th>
			<th colspan="3" class="mm-3">Ashad</th>
			<th colspan="3" class="mm-4">Sawran</th>
			<th colspan="3" class="mm-5">Bhadra</th>
			<th colspan="3" class="mm-6">Asoj</th>
			<th colspan="3" class="mm-7">Kartik</th>
			<th colspan="3" class="mm-8">Mushir</th>
			<th colspan="3" class="mm-9">Poush</th>
			<th colspan="3" class="mm-10">Magh</th>
			<th colspan="3" class="mm-11">Falgun</th>
			<th colspan="3" class="mm-12">Chitra</th>
			<th colspan="3" class="mm-fs">Festival</th>
		</tr>
		<tr>
			<th class="mm-1">Date</th>
			<th class="mm-1">Bill No.</th>
			<th class="mm-1">Amount</th>
			<th class="mm-2">Date.</th>
			<th class="mm-2">Bill No.</th>
			<th class="mm-2">Amount</th>
			<th class="mm-3">Date.</th>
			<th class="mm-3">Bill No..</th>
			<th class="mm-3">Amount</th>
			<th class="mm-4">Date.</th>
			<th class="mm-4">Bill No.</th>
			<th class="mm-4">Amount</th>
			<th class="mm-5">Date.</th>
			<th class="mm-5">Bill No.</th>
			<th class="mm-5">Amount</th>
			<th class="mm-6">Date.</th>
			<th class="mm-6">Bill No.</th>
			<th class="mm-6">Amount</th>
			<th class="mm-7">Date.</th>
			<th class="mm-7">Bill No.</th>
			<th class="mm-7">Amount</th>
			<th class="mm-8">Date.</th>
			<th class="mm-8">Bill No.</th>
			<th class="mm-8">Amount</th>
			<th class="mm-9">Date.</th>
			<th class="mm-9">Bill No.</th>
			<th class="mm-9">Amount</th>
			<th class="mm-10">Date.</th>
			<th class="mm-10">Bill No.</th>
			<th class="mm-10">Amount</th>
			<th class="mm-11">Date.</th>
			<th class="mm-11">Bill No.</th>
			<th class="mm-11">Amount</th>
			<th class="mm-12">Date.</th>
			<th class="mm-12">Bill No.</th>
			<th class="mm-12">Amount</th>
			<th class="mm-fs">Date.</th>
			<th class="mm-fs">Bill No.</th>
			<th class="mm-fs">Amount</th>
		</tr>
	</thead>
	<tbody>
		@foreach($datas as $data)
			<tr>
				<td>{{$data->customer_name}}</td>
				<td>{{$data->customer_code}}</td>
				<td class="mm-1">{{$data->date_1}}</td>
				<td class="mm-1">{{$data->bill_1}}</td>
				<td class="mm-1">{{$data->amount_1}}</td>
				<td class="mm-2">{{$data->date_2}}</td>
				<td class="mm-2">{{$data->bill_2}}</td>
				<td class="mm-2">{{$data->amount_2}}</td>
				<td class="mm-3">{{$data->date_3}}</td>
				<td class="mm-3">{{$data->bill_3}}</td>
				<td class="mm-3">{{$data->amount_3}}</td>
				<td class="mm-4">{{$data->date_4}}</td>
				<td class="mm-4">{{$data->bill_4}}</td>
				<td class="mm-4">{{$data->amount_4}}</td>
				<td class="mm-5">{{$data->date_5}}</td>
				<td class="mm-5">{{$data->bill_5}}</td>
				<td class="mm-5">{{$data->amount_5}}</td>
				<td class="mm-6">{{$data->date_6}}</td>
				<td class="mm-6">{{$data->bill_6}}</td>
				<td class="mm-6">{{$data->amount_6}}</td>
				<td class="mm-7">{{$data->date_7}}</td>
				<td class="mm-7">{{$data->bill_7}}</td>
				<td class="mm-7">{{$data->amount_7}}</td>
				<td class="mm-8">{{$data->date_8}}</td>
				<td class="mm-8">{{$data->bill_8}}</td>
				<td class="mm-8">{{$data->amount_8}}</td>
				<td class="mm-9">{{$data->date_9}}</td>
				<td class="mm-9">{{$data->bill_9}}</td>
				<td class="mm-9">{{$data->amount_9}}</td>
				<td class="mm-10">{{$data->date_10}}</td>
				<td class="mm-10">{{$data->bill_10}}</td>
				<td class="mm-10">{{$data->amount_10}}</td>
				<td class="mm-11">{{$data->date_11}}</td>
				<td class="mm-11">{{$data->bill_11}}</td>
				<td class="mm-11">{{$data->amount_11}}</td>
				<td class="mm-12">{{$data->date_12}}</td>
				<td class="mm-12">{{$data->bill_12}}</td>
				<td class="mm-12">{{$data->amount_12}}</td>
				<td class="mm-fs">{{$data->festival_date}}</td>
				<td class="mm-fs">{{$data->festival_bill}}</td>
				<td class="mm-fs">{{$data->festival_amount}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

