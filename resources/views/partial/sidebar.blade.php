<div id="sidebar" class="sidebar responsive ace-save-state">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>

	<ul class="nav nav-list">
		<li >
			<a href="{{asset('dashboard')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Dashboard </span>
			</a>

			<b class="arrow"></b>
		</li>

		

		<li class="">
			<a href="{{asset('marga')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Marga </span>
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="{{asset('designation')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Designation </span>
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="{{asset('staff')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Staff</span>
			</a>

			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="{{asset('customer')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Customer</span>
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="#" class="dropdown-toggle">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text">Account</span>

				<b class="arrow fa fa-angle-down"></b>
			</a>

			<b class="arrow"></b>

			<ul class="submenu">
				<li class="">
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-hand-o-right"></i>
						Salary
						<b class="arrow fa fa-angle-down"></b>
					</a>

					<b class="arrow"></b>

					<ul class="submenu">
						<li class="">
							<a href="{{asset('salary/advsalary')}}">
								<i class="menu-icon fa fa-caret-right"></i>
								Advance
							</a>

							<b class="arrow"></b>
						</li>

						<li class="">
							<a href="{{asset('salary')}}">
								<i class="menu-icon fa fa-caret-right"></i>
								Salary
							</a>

							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="">
					<a href="{{asset('bank')}}">
						<i class="fa fa-hand-o-right"></i>
						Bank
					</a>

					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="{{asset('expenses')}}">
						<i class="fa fa-hand-o-right"></i>
						Expenses
					</a>

					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="{{asset('collection')}}">
						<i class="fa fa-hand-o-right"></i>
						Collection
					</a>

					<b class="arrow"></b>
				</li>
			</ul>
		</li>

		<li class="">
			<a href="#" class="dropdown-toggle">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text">Bill</span>

				<b class="arrow fa fa-angle-down"></b>
			</a>

			<b class="arrow"></b>

			<ul class="submenu">
				<li class="">
					<a href="{{asset('bill/post_bill')}}">
						<i class="fa fa-hand-o-right"></i>
						Post Bills
					</a>

					<b class="arrow"></b>
				</li>

				<li class="">
					<a href="{{asset('bill')}}">
						<i class="fa fa-hand-o-right"></i>
						View Bills
					</a>

					<b class="arrow"></b>
				</li>

				<li class="">
					<a href="{{asset('festivalbill/post_bill')}}">
						<i class="fa fa-hand-o-right"></i>
						Post Festival Bill
					</a>

					<b class="arrow"></b>
				</li>

				<li class="">
					<a href="{{asset('festivalbill')}}">
						<i class="fa fa-hand-o-right"></i>
						View Festival Bill
					</a>

					<b class="arrow"></b>
				</li>
			</ul>
			<li class="">
				<a href="">
					<i class="fa fa-hand-o-right"></i>
					<span class="menu-text"> Report</span>
				</a>

				<b class="arrow"></b>
			</li>
		</li>

	</ul><!-- /.nav-list -->

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>