@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('customer')}}">List Customer</a>
		</li>
		<li>
			<a href="">Create New Customer</a>
		</li>
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Succesfully !! &nbsp;</strong>{{Session::get('success')}}
	</div>
	@endif

	@if(Session::get('error'))
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Error !! &nbsp;</strong>{{Session::get('error')}}
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="well">
				<div class="table-header">
					Donates Required Field
				</div>
				<div class="space space-8"></div>
				<form action="{{asset('customer/save_customer')}}" method="POST" role='form' class="form-horizontal" enctype="multipath/form-data" id="add_customer">
					{{csrf_field()}}
					<fieldset>
						<div class="form-group">
							<label class="control-label col-md-2" for="name">
								Name
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="text" autocomplete="off" id="name" name="name" class="form-control" required="required" value="{{old('name')}}" />
								<span class="text-danger">{{$errors->first('name')}}</span>
							</div>
							<label class="control-label col-md-2">Join Date<span style="color: red;">*</span></label>								
							<div class="col-md-4">
								<input  type="text" class="form-control date-picker" id="id-date-picker-1" name="date" placeholder="yyyy-mm-dd" required />
								<span class="text-danger">{{$errors->first('date')}}</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="phone">
								Mobile
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" name="mobile" value="{{old('mobile')}}" id="mobile" class="form-control" required="required">	
								<span class="text-danger">{{$errors->first('mobile')}}</span>
							</div>
							<label class="control-label col-md-2" for="phone">
								Phone
							</label>
							<div class="col-md-4">
								<input type="number" name="phone" id="phone" value="{{old('phone')}}" autocomplete="off" class="form-control" placeholder="Optional">	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								House No.
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="text" autocomplete="off" value="{{old('house_no')}}" name="house_no" id="house_no" class="form-control" required="required">
								<span class="text-danger">{{$errors->first('house_no')}}</span>
							</div>
							<label class="control-label col-md-2">Ward No.<span style="color: red;">*</span></label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" value="{{old('ward_no')}}" name="ward_no" id="ward_no" class="form-control" required="required">
								<span class="text-danger">{{$errors->first('ward_no')}}</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Code No.
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="text" class="form-control" value="{{old('code_no')}}" name="code_no" id="code_no" required="required">
								<span class="text-danger">{{$errors->first('code_no')}}</span>
							</div>
							<label class="control-label col-md-2" for="salary">
								Customer ID.
							</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="customer_id" id="customer_id" 
								value="{{$customer_id+1}}"  readonly="">
							</div>	
							
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Marga
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<select class="chosen-select form-control" id="marga" name="marga" required="required">
									<option value="" disabled="disable" selected="selected">Please select</option>
									@foreach($margas as $marga)
									<option value="{{$marga->id}}">{{$marga->name}} / Staff => {{$marga->staffs['name']}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{$errors->first('marga')}}</span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-2" for="med_allowance">
								No. of Floors
							</label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" value="{{old('no_of_floors')}}" id="no_of_floors" name="no_of_floors" class="form-control">
							</div>
							<label class="control-label col-md-2" for="com_allowance">
								No. of Kitchens
							</label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" value="{{old('no_of_kitchen')}}" id="no_of_kitchen" name="no_of_kitchen" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="waste_mgmt">
								No. of shutters
							</label>
							<div class="col-md-4">
								<input type="text" autocomplete="off" value="{{old('no_of_shutter')}}" id="no_of_shutter" name="no_of_shutter" class="form-control" >
							</div>
							<label class="control-label col-md-2">
								Monthly Charges
								<span style="color: red;">*</span>
							</label>
							<div class="col-md-4">
								<input type="number" autocomplete="off" value="{{old('monthly_charge')}}" id="monthly_charge" name="monthly_charge" class="form-control" arge"="" required="required">
							</div>
							<hr>
							<hr>
							<div class="col-md-8 col-md-offset-4" >
								<div class="col-md-4">
									<input type="submit" name="save_staff" value="Save" class="btn btn-success col-md-12" >
								</div>
								<div class="col-md-4">
									<a href="{{asset('staff')}}" class="btn btn-danger col-md-12">Cancel</a>
								</div>
							</div>
						</div>
					</fieldset>
					
				</form>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>
@endsection
@push('script')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>

	$('.date-picker').datepicker({
		format:'yyyy-mm-dd',
		autoclose: true,
		todayHighlight:true
	})

	if(!ace.vars['touch']) {
		$('.chosen-select').chosen({allow_single_deselect:true});
	}
// function selectmarge() {
// 	var x = $('#marga').val();
// 	$.ajax({
// 		url:"{{asset('passmargaid')}}/"+x,
// 		type: "get",
// 		success:function(response){
// 			$('#incharge').val(response.id);
// 		}
// 	});
// }
$('#add_customer').validate({
	errorElement: 'div',
	errorClass: 'help-block',
	highlight: function (e) {
		$(e).closest('.col-md-4').removeClass('has-info').addClass('has-error');
	},
});

</script>
@endpush
