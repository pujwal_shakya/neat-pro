@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="">List Customer</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
            <div class="well">
                <a href="{{asset('customer/add_customer')}}" class="btn btn-success">
                    <i class="fa fa-plus"></i>Add New Customer
                </a>
            <!-- <button role="button" class="btn btn-info">
              <i class="fa fa-print"></i>
              Print Preview
            </button>
            <button role="button" class="btn btn-warning">
            <i class=" fa fa-external-link"></i>
              Export
          </button> -->
          <div class="pull-right">
            <h4 class="label label-xlg label-white middle">Total Customer: {{$customers->count()}}</h4>
        </div>
        <div class="hr hr-double hr-dotted hr18"></div>
        <div class="col-md-12 well">
            <legend> SEARCH CUSTOMER</legend>
            <form>
                <fieldset>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-sm-3">Code No:</label>
                                <input type="text" class="col-sm-9 ui-autocomplete-input" name="search_by_code" id="search_by_code" placeholder="search by code no" autocomplete="off" onkeyup="filterTable()">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-sm-3">Name:</label>
                                <input type="text" class="col-sm-9 ui-autocomplete-input" name="search_by_name" id="search_by_name" placeholder="search by name" autocomplete="off" onkeyup="filterTable()">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="search_by_staff" id="search_by_staff" class="chosen-select form-control" 
                                onchange="filterTable()">
                                    <option value="">All Staff</option>
                                    @foreach($staffs as $staff)
                                    <option value="{{$staff->id}}">{{$staff->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="search_by_marga" id="search_by_marga" class="chosen-select form-control" 
                                onchange="filterTable()">
                                    <option value="">All Marga</option>
                                    @foreach($margas as $marga)
                                    <option value="{{$marga->id}}">{{$marga->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="table" id="list-customer">
            <table class="table table-striped table-bordered table-hover table-resposnive" id="customer">
                <thead>
                    <tr>
                        <!-- <th>Customer Id</th> -->
                        <th>Code No.</th>
                        <th>Customer Name</th>
                        <th>Phone No.</th>
                        <th>Mobile No.</th>
                        <th>Monthly Charge</th>
                        <th>Collector</th>
                        <th>Marga</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div><!-- /.row -->
</div>


<!-- Edit Modal starts -->

<!-- Edit Modal Ends -->

@endsection
@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script>
    var url="{{asset('customer/getdata')}}";
    var table=$('#customer').DataTable({
        ajax:url,
        columns:[
        {data:'code_no'},   
        {data:'name'},   
        {data:'phone'},   
        {data:'mobile'},   
        {data:'monthly_charge'},   
        {data:'margas.staffs.name'},
        {data:'margas.name'},
        {data:'status'},
        {data:'action',orderable:false,searchable:false}
        ],
    });
    function filterTable(){
        var code_no=$('#search_by_code').val();
        var c_name=$('#search_by_name').val();
        var s_name=$('#search_by_staff').val();
        var m_name=$('#search_by_marga').val();
        table.ajax.url(url+'?code_no='+code_no+'&c_name='+c_name+'&s_name='+s_name+'&m_name='+m_name).load();
    }
    
</script>

@endpush