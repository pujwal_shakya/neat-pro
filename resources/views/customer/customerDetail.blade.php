@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('customer')}}">List Customer</a>
		</li>
		<li>
			<a href="">Customer Detail</a>
		</li>
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	@if(Session::get('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Succesfully !! &nbsp;</strong>{{Session::get('success')}}
	</div>
	@endif

	@if(Session::get('error'))
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Error !! &nbsp;</strong>{{Session::get('error')}}
	</div>
	@endif
	
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="well">
				<div class="table-header">
					Donates Required Field\
					{!!implode(',',$errors->all())!!}
				</div>
				<div class="space space-8"></div>
				<form action="{{asset('customer/editdetail')}}/{{$customers->id}}" method="POST" role='form' class="form-horizontal" enctype="multipath/form-data">
					{{csrf_field()}}
					<fieldset>
						<div class="form-group">
							<label class="control-label col-md-2" for="name">
								Name
							</label>
							<div class="col-md-4">
								<input type="text" value="{{$customers->name}}" id="name" name="name" class="form-control" required="required" />
							</div>
							<label class="control-label col-md-2">Join Date</label>								
							<div class="col-md-4">
								<input class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" value="{{$customers->date}}" name="date" placeholder="yyyy-mm-dd" />
							</div>
							
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="phone">
								Mobile
							</label>
							<div class="col-md-4">
								<input type="number" value="{{$customers->mobile}}" name="mobile" id="mobile" class="form-control" required="required">
								<span class="text-danger">{{$errors->first('mobile')}}</span>	
							</div>
							<label class="control-label col-md-2" for="phone">
								Phone
							</label>
							<div class="col-md-4">
								<input type="number" name="phone" id="phone" value="{{$customers->phone}}" class="form-control" placeholder="Optional">	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								House No.
							</label>
							<div class="col-md-4">
								<input type="text" value="{{$customers->house_no}}" name="house_no" id="house_no" class="form-control" required="required">
								<span class="text-danger">{{$errors->first('house_no')}}</span>
							</div>
							<label class="control-label col-md-2">Ward No.</label>
							<div class="col-md-4">
								<input type="number" value="{{$customers->ward_no}}" name="ward_no" id="ward_no" class="form-control" required="required">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Code No.
							</label>
							<div class="col-md-4">
								<input type="text" class="form-control" value="{{$customers->code_no}}" name="code_no" id="code_no" required="required">
								<span class="help-block red">{{$errors->first('code_no')}}</span>
							</div>
							
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">
								Marga
							</label>
							<div class="col-md-4">
								<select class="chosen-select form-control" id="marga" name="marga">
									@foreach($margas as $marga)
									<option value="{{$marga->id}}" {{($customers->margas['id']==$marga->id)?'selected':''}}>{{$marga->name}} / Collector 
										=>
										@if(!empty($marga->staffs['name']))
											{{$marga->staffs['name']}}
										@else
											No Staff Assigned
										@endif
									</option>
									@endforeach
								</select>
							</div>
						</div>							
						<div class="form-group">
							<label class="control-label col-md-2" for="med_allowance">
								No. of Floors
							</label>
							<div class="col-md-4">
								<input type="number" value="{{$customers->floor_no}}" id="no_of_floors" name="no_of_floors" class="form-control">
							</div>
							<label class="control-label col-md-2" for="com_allowance">
								No. of Kitchens
							</label>
							<div class="col-md-4">
								<input type="number" value="{{$customers->kitchen_no}}" id="no_of_kitchen" name="no_of_kitchen" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2" for="waste_mgmt">
								No. of shutters
							</label>
							<div class="col-md-4">
								<input type="text" value="{{$customers->shutter_no}}" id="no_of_shutter" name="no_of_shutter" class="form-control" >
							</div>
							<label class="control-label col-md-2">
								Monthly Charges
							</label>
							<div class="col-md-4">
								<input type="number" value="{{$customers->monthly_charge}}" id="monthly_charge" name="monthly_charge" class="form-control" arge"="" required="required">
							</div>
							<hr>
							<hr>
							<div class="col-md-8 col-md-offset-4" >
								<div class="col-md-4">
									<input type="submit" name="save_staff" value="Save" class="btn btn-success col-md-12" >
								</div>
								<div class="col-md-4">
									<a href="{{asset('customer')}}" class="btn btn-danger col-md-12">Cancel</a>
								</div>
							</div>
						</div>
					</fieldset>
					
				</form>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>
@endsection
@push('script')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script>


	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight:true
	})
	if(!ace.vars['touch']) {
		$('.chosen-select').chosen({allow_single_deselect:true});
	}
// function selectmarga(){
// 	var x = $('#marga').val();
// 	$.ajax({
// 		url:"{{asset('passmargaid')}}/"+x,
// 		type:'get',
// 		success:function(response){
// 			$('#incharge').val(response.name);
// 		}
// 	});

// }
</script>
@endpush
