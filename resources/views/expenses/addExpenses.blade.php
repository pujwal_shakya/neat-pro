@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Home</a>
        </li>
        <li>
            <a href="{{asset('expenses')}}">Expenses</a>
        </li>
        <li>
            <a href="">Add Expenses</a>
        </li> 
    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
    @if(Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Succesfully !! &nbsp;</strong>{{Session::get('success')}}
    </div>
    @endif

    @if(Session::get('error'))
    <div class="alert alert-error">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error !! &nbsp;</strong>{{Session::get('error')}}
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="table-header">Add Expenses</div>
            <div class="well">
            <form role="form" class="form-horizontal" action="{{asset('expenses/submit')}}" method="post" id="exp_table">
                {{csrf_field()}}
                <fieldset>
                    <input type="hidden" value="" name="id">
                    <div class="form-group">
                        <label class="control-label col-md-2" for="full_name">Date<span style="color: red;">*</span></label>
                        <div class="col-md-4">
                            <input type="text" id="nepaliDate" name="date" class="form-control date-picker"  required="required">
                        </div>
                        <label class="control-label col-md-2" for="v_no">Voucher No.<span style="color: red;">*</span></label>
                        <div class="col-md-4">
                           <input type="text" autocomplete="off" name="v_no" id="v_no" class="form-control" required>
                           <span class="text-danger">{{$errors->first('v_no')}}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="title">Title<span style="color: red;">*</span></label>
                        <div class="col-md-4">
                             <input type="text" autocomplete="off" name="title" id="title" class="form-control" placeholder="" required>
                        </div>
                        <label class="control-label col-md-2" for="amount">Amount<span style="color: red;">*</span></label>
                        <div class="col-md-4">
                           <input type="text" autocomplete="off" name="amount" id="amount" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="full_name">Particulars</label>
                        <div class="col-md-10">
                            <textarea class="form-control" name="particulars"></textarea>
                        </div>
                    </div>
                    <div class="hr hr-double hr-dotted hr18"></div>
                    <div class="col-md-8 col-md-offset-4">
                        <div class="col-md-4">
                            <input type="submit" class="btn btn-success col-md-12" name="save_expenses" value="Save">
                        </div>
                        <div class="col-md-4">
                            <a href="http://andeep/projects/neatpro/expenses" class="btn btn-danger col-md-12">Cancel</a>
                        </div>
                    </div>
                </fieldset>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
    $('.date-picker').datepicker({
        format:'yyyy-mm-dd',
        autoclose: true,
        todayHighlight:true
    })
    $('#exp_table').validate({
    errorElement: 'div',
    errorClass: 'help-block',
    highlight: function (e) {
        $(e).closest('div').removeClass('has-info').addClass('has-error');
    },
});
</script>
@endpush