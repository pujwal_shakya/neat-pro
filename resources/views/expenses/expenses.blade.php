@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Home</a>
        </li>

        <li>
            <a href="{{asset('marga')}}">Expenses</a>
        </li>
        
    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
    <div class="col-xs-12">
    	<div class="well">
          <a href="{{asset('expenses/add_exp')}}" role="button" class="btn btn-success" data-toggle="modal">
            <i class="fa fa-plus"></i>
            Add Expenses
          </a>
          <div class="hr hr-double hr-dotted hr18"></div>
          <div id="list_search_result"><!--  -->
            <div id="list_result">
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>S.N</th>
                    <th>Date</th>
                    <th>Voucher No.</th>
                    <th>Title</th>
                    <th>Amount</th>
                    <th>Particulars</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($expenses as $key=>$expense)
                  <tr id="{{$expense->id}}">
                    <td>{{$key+1}}</td>
                    <td>{{$expense->date}}</td>
                    <td>{{$expense->v_no}}</td>
                    <td>{{$expense->title}}</td>
                    <td>{{$expense->amount}}</td>
                    <td>{{$expense->particular}}</td>
                    <td>
                        <a href="{{asset('expenses/edit')}}/{{$expense->id}}" class="edit-faculty btn btn-xs btn-warning">
                          <span class="glyphicon glyphicon-eye-open"></span>
                          Edit
                        </a>
                        <a role="button" class="btn btn-xs btn-danger remove_marga" 
                        onclick="Delete({{$expense->id}})">
                            <span class="ace-icon fa fa-trash-o bigger-120"></span>
                            Delete
                        </a>
                    </td>
                  </tr>   
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('script')
<script>
  function Delete(id){
    if(confirm('Are You Sure Want To Remove?')==false){
        return false;
    }else{
      $.ajax({
        url:"{{asset('expenses/delete')}}/"+id,
        type:'get',
        success:function(response){
            $('#'+id).remove();
        }
      });
    }
  }
</script>
@endpush