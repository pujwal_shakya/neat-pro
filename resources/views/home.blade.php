<div class="col-md-12 well">

    <legend> SEARCH</legend>

    <form>
        <fieldset>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-sm-4">Staff Id</label>
                        <input type="text" class="col-sm-8" name="staff_id" id="staff_id" placeholder="staff id">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-3">Name:</label>
                        <input type="text" class="col-sm-9" name="staff_name" id="staff_name" placeholder="search by name">
                    </div>
                </div>
                <div class="space-24"></div>
            </div>
        </fieldset>
    </form>
</div>