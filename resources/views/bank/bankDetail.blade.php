@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="">Bank Details</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12 well">
			<div class="table">
				<ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
					@foreach($staffs as $staff)
					<li class="">
						<a href="{{asset('bank/deposit_details')}}/{{$staff->id}}">
							<i class="blue ace-icon fa fa-folder  bigger-120"></i>
						{{$staff->name}}                              
						</a>
					</li>
					@endforeach
				</ul>
			</div>
			<!-- calculation -->
			
			<!-- end calculation -->
			<div class="space space-8"></div>
			<div class="col-xs-12" id="list_search_result">
				<div class=" col-sm-4 widget-container-col">
					<div class="widget-box">
						<div class="widget-header">
							<h5 class="widget-title">TOTAL AMOUNT IN BANK</h5>
						</div>
						<div class="widget-body">
							
							<div class="widget-main">
								<div>
									<h1 class="center">
										<?php
										$total=$deposit-$withdraw;
										?>
										{{$total}}
									</h1>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class=" col-sm-4 widget-container-col">
					<div class="widget-box ">
						<div class="widget-header">
							<h5 class="widget-title">TOATAL DEPOSIT AMOUNT</h5>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								
								<h1 class="center">{{$deposit}}</h1>
								
							</div>
						</div>
					</div>
				</div>

				<div class=" col-sm-4 widget-container-col">
					<div class="widget-box">
						<div class="widget-header">
							<h5 class="widget-title">TOTAL WITHDRAW AMOUNT</h5>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								
								<h1 class="center">{{$withdraw}}</h1>
								
								<h1 class="center"></h1>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>
<div class="col-xs-12 well">
			<h3 class="col-md-12 center"> Bank Transaction</h3>
            
            <div class="hr hr-double hr-dotted hr18"></div>
				<div class=" well">
					<div id="list_search_result" class="table-responsive"><!--  -->
						<table class="table table-striped table-bordered table-hover" id="transaction_table">
							<thead>
								<tr>
									
									<th>Date</th>
									<th>Transaction By</th>
									<th>Bank Name</th>
									<th>Branch</th>
									<th>Account No.</th>
									<th>Account Name</th>
									<th>Account Type</th>
									<th>Deposit Type</th>
									<th>Transaction</th>
									<th>Amount</th>
									<th>Remarks</th>
								</tr>
							</thead>
							
						</table>
					</div>
				</div>

            <div class="space space-8"></div>
</div>
		</div>
	</div><!-- /.row -->
</div>
@endsection

@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script>
	$(document).ready(function(){
		var url="{{asset('bank/data')}}";
		var currentURL= window.location.href;
		var params=currentURL.split("?");
		var query=params[1];
		$('#transaction_table').dataTable({
			ajax:url,
			columns:[
				{data:'date'},
				{data:'staffs.name'},
				{data:'bank_name'},
				{data:'branch'},
				{data:'acc_no'},
				{data:'acc_name'},
				{data:'acc_type'},
				{data:'deposit_type'},
				{data:'transaction'},
				{data:'amount'},
				{data:'remarks'},
			],
		});
	});
</script>
@endpush