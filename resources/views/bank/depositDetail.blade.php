@extends('layout.app')
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="{{asset('bank')}}">Bank Details</a>
		</li>
		<li>
			<a href="">{{$staff->name}}</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12 well">
		    <div>
                <a href="{{asset('bank/post_bank')}}/{{$staff->id}}" class="btn btn btn-success"><i class="fa fa-plus "></i> Post New</a>
                <h3 class="center"> Bank Transaction of {{$staff->name}}</h3>
            </div>
            <div class="hr hr-double hr-dotted hr18"></div>
				<div class=" well">
					<div id="list_search_result" class="table-responsive"><!--  -->
						<table class="table table-striped table-bordered table-hover" id="transaction_table">
							<thead>
								<tr>
									
									<th>Date</th>
									<th>Bank Name</th>
									<th>Branch</th>
									<th>Account No.</th>
									<th>Account Name</th>
									<th>Account Type</th>
									<th>Deposit Type</th>
									<th>Transaction</th>
									<th>Amount</th>
									<th>Remarks</th>
								</tr>
							</thead>
							
						</table>
					</div>
				</div>

            <div class="space space-8"></div>
		</div>
	</div><!-- /.row -->
</div>

@endsection

@push('script')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/dataTables.select.min.js')}}"></script>
<script>
	$(document).ready(function(){
		var url="{{asset('bank/staff/data')}}?name="+{{$staff->id}};
		var currentURL= window.location.href;
		var params=currentURL.split("?");
		var query=params[1];
		$('#transaction_table').dataTable({
			ajax:url,
			columns:[
				{data:'date'},
				{data:'bank_name'},
				{data:'branch'},
				{data:'acc_no'},
				{data:'acc_name'},
				{data:'acc_type'},
				{data:'deposit_type'},
				{data:'transaction'},
				{data:'amount'},
				{data:'remarks'},
			],
		});
	});
</script>
@endpush