@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="{{asset('dashboard')}}">Home</a>
		</li>

		<li>
			<a href="">Add Transaction</a>
		</li>
		
	</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="page-header">

	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-md-12">
			<div class="table-header"><em style="color:red;">*</em> [ Denotes Mandatory Fields ]</div>
			<div class="well">
				<div>
					<form role="form" class="form-horizontal" action="{{asset('bank/save_transacion')}}" method="post" id="transaction_form">
						{{csrf_field()}}
						<fieldset>
							<input type="hidden" name="id" value="">
							<div class="form-group">
								<label class="control-label col-md-2" for="bank_name">Bank Name<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<input type="text" id="bank_name" name="bank_name" class="form-control ui-autocomplete-input" required="" autocomplete="off">
								</div>
								<label class="control-label col-md-2" for="branch">Branch<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<input type="text" id="branch" name="branch" class="form-control ui-autocomplete-input" required=""  autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Date<span style="color: red;">*</span></label>
									<div class="col-md-4">
										<input type="text" name="date" class="form-control date-picker" id="date" required>
										<span class="text-danger">{{$errors->first('date')}}</span>
									</div>
								<label class="control-label col-md-2" for="acc_no">Account No.<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<input type="text" autocomplete="off" name="acc_no" id="acc_no" class="form-control ui-autocomplete-input" required>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="acc_name">Account Name<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<input type="text" autocomplete="off" id="acc_name" name="acc_name" class="form-control" required="required">
								</div>

								<label class="control-label col-md-2" for="acc_type">Account Type<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<input type="text" autocomplete="off" id="acc_type" name="acc_type" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="acc_name">Deposit Type<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<div class="radio">
										<label>
											<input name="deposit_type" value="cash" type="radio" id="deposit_type" class="ace" checked>
											<span class="lbl"> Cash</span>
										</label>
										<label>
											<input name="deposit_type" value="cheque" id="deposit_type" type="radio" class="ace">
											<span class="lbl"> Cheque</span>
										</label>
									</div>
								</div> 

								<label class="control-label col-md-2" for="acc_type">Deposit|Withdraw<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<div class="radio">
										<label>
											<input name="transaction" id="transaction" type="radio" class="ace" checked="" value="deposit">
											<span class="lbl"> Deposit</span>
										</label>
										<label>
											<input name="transaction" value="withdraw" id="transaction" type="radio" class="ace">
											<span class="lbl"> Withdraw</span>
										</label>
									</div>

								</div> 
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="deposit_by">Deposit|Withdraw By<span style="color: red;">*</span></label>
								<div class="col-md-4">
									<input type="hidden" name="transaction_by" value="{{$staff->id}}">
									<input type="text" autocomplete="off" id="deposit_by" name="" class="form-control" placeholder="Deposit by name" value="{{$staff->name}}" readonly="">
								</div>

								<label class="control-label col-md-2" for="deposit_amount">Amount<span style="color: red;">*</span> </label>
								<div class="col-md-4">
									<input type="text" autocomplete="off" id="deposit_amount" name="amount" class="form-control" required>
									<!-- <textarea class="form-control" name="remarks" id="remarks"></textarea> -->
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="acc_name">Remarks</label>
								<div class="col-md-10">
									<textarea class="form-control" name="remarks"></textarea>
								</div>
							</div>

							<div class="col-md-8 col-md-offset-4">
								<div class="col-md-4">
									<input type="submit" class="btn btn-success col-md-12" name="save_bank" value="Post Transaction">
								</div>
								<div class="col-md-4">
									<a href="http://andeep/projects/neatpro/bank" class="btn btn-danger col-md-12">Cancel</a>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
	$('.date-picker').datepicker({
		format:'yyyy-mm-dd',
		autoclose: true,
		todayHighlight:true
	})
	$('#transaction_form').validate({
	errorElement: 'div',
	errorClass: 'help-block',
	highlight: function (e) {
		$(e).closest('.col-md-4').removeClass('has-info').addClass('has-error');
	},
});
</script>
@endpush