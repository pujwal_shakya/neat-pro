<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_name');
            $table->string('branch');
            $table->integer('acc_no');
            $table->string('acc_name');
            $table->string('acc_type');
            $table->string('deposit_type');
            $table->string('transaction');
            $table->integer('staff_id');
            $table->string('amount');
            $table->string('remarks');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_bank');
    }
}
