<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Collection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_collection', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('bill_from');
            $table->integer('bill_to');
            $table->string('dr_amt')->nullable();
            $table->string('cr_amt')->nullable();
            $table->string('balance');
            $table->string('previous_collection')->nullable();
            $table->string('no_of_bill');
            $table->integer('staff_id')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_collection');
    }
}
