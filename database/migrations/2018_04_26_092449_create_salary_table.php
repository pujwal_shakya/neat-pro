<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id');
            $table->string('staff_name');
            $table->string('designation');
            $table->string('salary');
            $table->string('performance_allowance')->nullable();
            $table->string('expensive_allowance')->nullable();
            $table->string('medical_allowance')->nullable();
            $table->string('communication_allowance')->nullable();
            $table->string('waste_mgmt')->nullable();
            $table->string('advance')->nullable();
            $table->string('pro_fund')->nullable();
            $table->string('tax')->nullable();
            $table->string('paid_up_salary');
            $table->string('date');
            $table->string('bonus_amount')->nullable();
            $table->string('comission_amount')->nullable();
            $table->string('leave_amount')->nullable();
            $table->string('food_amount')->nullable();
            $table->string('overtime_amount')->nullable();
            $table->string('grand_total');
            $table->string('remarks')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary');
    }
}
