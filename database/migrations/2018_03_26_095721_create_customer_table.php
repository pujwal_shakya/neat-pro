<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_no');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('mobile');
            $table->string('house_no');
            $table->string('ward_no');
            $table->string('marga_id')->nullable();
            $table->string('kitchen_no')->nullable();
            $table->string('shutter_no')->nullable();
            $table->string('floor_no')->nullable();
            $table->string('monthly_charge');
            $table->string('status');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
