<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_annual_report', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_code');
            $table->integer('customer_id');
            $table->string('customer_name');
            $table->integer('marga_id');
            $table->integer('staff_id');
            $table->integer('bill_1')->nullable();
            $table->date('date_1')->nullable();
            $table->string('amount_1')->nullable();
            $table->integer('bill_2')->nullable();
            $table->date('date_2')->nullable();
            $table->string('amount_2')->nullable();
            $table->integer('bill_3')->nullable();
            $table->date('date_3')->nullable();
            $table->string('amount_3')->nullable();
            $table->integer('bill_4')->nullable();
            $table->date('date_4')->nullable();
            $table->string('amount_4')->nullable();
            $table->integer('bill_5')->nullable();
            $table->date('date_5')->nullable();
            $table->string('amount_5')->nullable();
            $table->integer('bill_6')->nullable();
            $table->date('date_6')->nullable();
            $table->string('amount_6')->nullable();
            $table->integer('bill_7')->nullable();
            $table->date('date_7')->nullable();
            $table->string('amount_7')->nullable();
            $table->integer('bill_8')->nullable();
            $table->date('date_8')->nullable();
            $table->string('amount_8')->nullable();
            $table->integer('bill_9')->nullable();
            $table->date('date_9')->nullable();
            $table->string('amount_9')->nullable();
            $table->integer('bill_10')->nullable();
            $table->date('date_10')->nullable();
            $table->string('amount_10')->nullable();
            $table->integer('bill_11')->nullable();
            $table->date('date_11')->nullable();
            $table->string('amount_11')->nullable();
            $table->integer('bill_12')->nullable();
            $table->date('date_12')->nullable();
            $table->string('amount_12')->nullable();
            $table->integer('festival_bill')->nullable();
            $table->date('festival_date')->nullable();
            $table->string('festival_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_annual_report');
    }
}
