<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFestivalBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_festival', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('customer_code');
            $table->string('customer_name');
            $table->integer('marga_id');
            $table->integer('staff_id');
            $table->integer('bill_no');
            $table->date('date');
            $table->string('paid_amount');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_festival');
    }
}
