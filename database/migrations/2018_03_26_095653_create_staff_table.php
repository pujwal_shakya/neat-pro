<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('gender');
            $table->string('phone');
            $table->string('address');
            $table->string('designation')->nullable();
            $table->string('salary');
            $table->string('performance_allowance')->nullable();
            $table->string('expensive_allowance')->nullable();
            $table->string('medical_allowance')->nullable();
            $table->string('communication_allowance')->nullable();
            $table->string('waste_mgmt')->nullable();
            $table->string('image')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
