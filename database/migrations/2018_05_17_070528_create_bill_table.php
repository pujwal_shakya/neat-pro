<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bill', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('customer_code');
            $table->string('customer_name');
            $table->integer('marga_id');
            $table->integer('staff_id');
            $table->integer('bill_no');
            $table->date('date');
            $table->string('paid_from');
            $table->string('paid_upto')->nullable();
            $table->string('monthly_rate');
            $table->string('paid_amount');
            $table->string('member_fee')->nullable();
            $table->string('card_fee')->nullable();
            $table->string('drain_clean_fee')->nullable();
            $table->string('late_fee')->nullable();
            $table->string('other_fee')->nullable();
            $table->string('service_charge')->nullable();
            $table->string('less_service_charge')->nullable();
            $table->string('less_discount')->nullable();
            $table->string('total');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_bill');
    }
}
