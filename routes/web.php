<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','mainController@index');
Route::get('dashboard','mainController@index');

/*marga controller*/
Route::get('marga','margaController@marga');
Route::post('marga/save','margaController@addMarga');
Route::post('marga/update','margaController@updateMarga');
Route::get('marga/delete/{id}','margaController@deleteMarga');

/*customer controller*/
Route::get('customer','customerController@customer');
Route::get('customer/add_customer','customerController@addCustomer');
Route::post('customer/save_customer','customerController@saveCustomer');
Route::get('customer/detail/{id}',['as'=>'customer.detail','uses'=>'customerController@customerDetail']);
Route::post('customer/editdetail/{id}','customerController@updateDetail');
Route::get('customer/remove/{id}',['as'=>'customer.delete','uses'=>'customerController@deleteCustomer']);
Route::get('passmargaid/{id}','customerController@margaId');
Route::get('customer/getdata','customerController@getData');

/*staff controller*/
Route::get('staff','staffController@staff');
Route::get('staff/add_staff','staffController@addStaff');
Route::post('staff/save_staff','staffController@saveStaff');
Route::get('staff/add_collector/{id}','staffController@collector');
Route::post('staff/add_collector/assignmarga/{id}','staffController@assignMarga');
Route::get('staff/assignNewMarga','staffController@assignNewMarga');
Route::get('staff/add_collector/removemarga/{id}','staffController@removeMarga');
Route::get('staff/detail/{id}','staffController@staffDetail');
Route::post('staff/editdetail/{id}','staffController@updateDetail');
Route::get('staff/remove/{id}','staffController@deleteStaff');

// Auth::routes();
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');
Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

// designation controller
Route::get('designation','designationController@index');
Route::post('designation/save','designationController@addDesignation');
Route::get('designation/remove/{id}','designationController@deleteDesignation');
Route::post('designation/update','designationController@updateDesignation');

// salary controller
Route::get('salary','salaryController@index');
Route::get('salary/add_salary','salaryController@addSalary');
Route::get('salary/staff_detail/{id}','salaryController@staffDetail');
Route::post('salary/submit','salaryController@storeSalary');
Route::get('salary/advsalary','salaryController@advSalary');
Route::get('salary/advsalary/post_advance','salaryController@addAdvSalary');
Route::get('salary/advsalary/staff_detail/{id}','salaryController@AdvStaffDetail');
Route::post('salary/advsalary/submit','salaryController@storeAdvSalary');
Route::get('salary/advsalary/delete/{id}','salaryController@deleteAdvSalary');
Route::get('salary/advsalary/edit/{id}','salaryController@editAdvSalary');
Route::post('salary/advsalary/update/{id}','salaryController@updateAdvSalary');
Route::get('searchname','salaryController@nameSearch');
Route::get('salary/data','salaryController@data');

// expenses controler
Route::get('expenses','expensesController@index');
Route::get('expenses/add_exp','expensesController@addExpenses');
Route::post('expenses/submit','expensesController@saveExpenses');
Route::get('expenses/delete/{id}','expensesController@delete');
Route::get('expenses/edit/{id}','expensesController@editExpenses');
Route::post('expenses/update/{id}','expensesController@updateExpenses');

/*bank section*/
Route::get('bank','bankController@index');
Route::get('bank/deposit_details/{id}','bankController@depositDetails');
Route::get('bank/post_bank/{id}','bankController@addTransaction');
Route::post('bank/save_transacion','bankController@saveTransaction');
Route::get('bank/staff/data','bankController@staffsData');
Route::get('bank/data','bankController@bankData');



// collection section
Route::get('collection','collectionController@index');
Route::get('collection/details/{id}','collectionController@collectionDetails');
Route::get('collection/post_collection/{id}','collectionController@postCollection');
Route::post('collection/saveCollection','collectionController@saveCollection');
Route::get('collection/staffs/data','collectionController@staffData');

// Bill Section
Route::get('bill/post_bill','billController@postBill');
Route::get('bill/customer_detail/{id}','billController@customerDetail');
Route::post('bill/save_bill_details','billController@saveBill');
Route::get('bill','billController@index');
Route::get('bill/getdata','billController@getData');
Route::get('bill/searchbyname','billController@searchByName');

// Festival Bill Section
Route::get('festivalbill/post_bill','festivalBillController@postBill');
Route::get('festivalbill/customer_detail/{id}','festivalBillController@customerDetail');
Route::post('festivalbill/save_bill_details','festivalBillController@saveBill');
Route::get('festivalbill','festivalBillController@index');
Route::get('festivalbill/getdata','festivalBillController@getData');

// Annual Report Section
Route::get('report','reportController@index');
Route::get('report/due_report','reportController@dueReport');
Route::post('report/due_report/getdata','reportController@getData');
Route::get('report/annual_report','reportController@annualReport');
Route::get('report/annual_report/getdata','reportController@annualData');
Route::get('report/collection_report','reportController@collectionReport');
